# Informancie

Informancie est un wiki personnel axé sur les sciences, la technique, la
pédagogie et l'informatique libre réalisé avec
[VimWiki](https://github.com/vimwiki/vimwiki). Le but est de regrouper des
connaissances éparses de manière structurée et organique, avec des outils
invitant à la prise de notes.

## Branches

Deux branches sont disponibles dans ce dépôt:
* `master` Mise à jours rares, contenu propre et prêt pour publication;
* `draft` Brouillon, édition continue, prises de notes qui ne sont pas
  néccessairement terminées. Comme cette branche est très souvent mise à jours,
  bien penser à effectuer un `git pull` avant de commiter.

## Outils

Les plugins Vim suivants sont néccessaires:
* [VimWiki](https://github.com/vimwiki/vimwiki) pour rédiger et parcourir le
  wiki;
* [Vinscripts](https://framagit.org/Informancien/vinscripts) pour les graphes
  et diagrammes texte.

`make` est utilisé pour gérer la copie locale du wiki, le `Makefile` à la racine
de ce dépôt fait appels aux scripts dans le dossier `scripts/`.

## Makefile

Le fichier `Makefile` à la racine de ce dépôt fourni des cibles facilitants la
gestion de la copie locale du wiki:
* `install` Installe ou met à jour la configuration des outils pour
  l'utilisateur courant;
* `uninstall` Supprime la configuration préalablement installée par
  l'utilisateur courant;
* `check_todos` Liste tous les TODOs trouvés dans le wiki;
* `check_links` Liste les liens morts présents de le wiki;
* `check` Exécute toutes les vérifications précédentes.

## Arborescence du dépôt

Les dossiers du dépôt sont organisés comme suit:
* `conf/` Configurations installées par le script Bash `scripts/conf_local`;
* `scripts/` Scripts de gestion de la copie locale du wiki, appelés par le 
  `Makefile`;
* `vimwiki/` Contenu du wiki:
	* `files/` Fichiers joints au wiki:
		* `docs/` Documents et livres numériques;
		* `img/` Images insérées dans les pages;
		* `src/` Fichiers de codes sources.

## Licences

L'intégralité du texte rédigé dans le cadre de ce wiki est sous licence
[CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), la
redistribution et modification sont autorisées, tant que ses droits sont
conservés et que les auteurs originaux sont crédités.

Tout autres contenus joints au wiki (documents, codes sources...) ont leurs
propres licences.
