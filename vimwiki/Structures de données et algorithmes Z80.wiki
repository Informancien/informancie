= Structures de données et algorithmes Z80 =

Récupération d'un pointeur dans une table non alignée à partir d'un indice:
{{{asm
	; a=indice de table
	ld hl,.table
	add a,l		; C=1 si débordement
	ld l,a		; poids faible
	ld a,h
	adc a,0		; poids fort corrigé
	ld h,a
}}}

Table circulaire de pointeurs, temps constant:
{{{asm
.start
	ld ix,.table            ; 4
	ld l,(ix+2)             ; 5
	ld h,(ix+3)             ; 5
	ld (.start+1),hl        ; 4
	ld l,(ix+0)             ; 5
	ld h,(ix+1)             ; 5...=28, HL=ptrX
	...
.table
	defw ptr1,$+2
	defw ptr2,$+2
	defw ptr3,$+2
	defw ptr4,.table
}}}

Table circulaire de pointeurs, temps constant, optimisé:
{{{asm
.start
	ld de,.table            ; 3
	ld a,(de):ld l,a:inc e  ; 2+1+1
	ld a,(de):ld h,a:inc e  ; 2+1+1, HL=ptrX
	ld a,(de)               ; 2
	ld (.start+1),a         ; 4...=17

align 256
.table ; max 256 valeurs
	defw ptr1:defb lo($+1)
	defw ptr2:defb lo($+1)
	defw ptr3:defb lo($+1)
	defw ptr4:defb lo(.table)
}}}

Table circulaire de valeurs, temps constant, optimisé:
{{{asm
.start
	ld hl,.table+1          ; 3
	ld a,(hl)               ; 2
	ld (.start+1),a         ; 4
	dec l:ld a,(hl)         ; 1+2...=12, A=valX
	...

align 256
.table ; max 256 valeurs
	defb val1,lo($+2)
	defb val2,lo($+2)
	defb val3,lo($+2)
	defb val4,lo(.table+1)
}}}

Tracé de lignes
	[[https://cpcrulez.fr/coding_amslive08-3D.htm]]
	[[https://rasmlive.amstrad.info/edit/HBK3QArQ2jRkcoAWQ]]
	[[https://rasmlive.amstrad.info/edit/AnPwSqmRv93338Lkh]]
