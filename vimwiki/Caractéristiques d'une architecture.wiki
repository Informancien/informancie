= Caractéristiques d'une architecture =

== Sécurité ==

Les néccéssités en terme de sécurité d'une architecture sont donnés par
l'acronyme *DICT*:
* Disponibilité;
* Intégrité;
* Confidentialité;
* Tracabilité.

La _Perte de Données Maximum Admissible_ (PDMA, _Recovery Point Objective_ en
anglais) défini exprime la quantité maximale d'informations qui peuvent être
perdue lors d'un incident.

=== Disponibilité ===

La _disponibilité_ indique la résislience du système à un sinitre, majeur ou
mineur, provoquant son arrêt momentané.

Elle est souvent exprimée en temps de panne maximum (downtime) ou en pourcentage
de temps de fonctionnement.

TODO Calculs de disponibilité

Le _RTO_ (Recovery Time Objective) est le délais maximal pour la reprise
d'activité suite à un incident.

=== Intégrité ===

L'_intégrité_ s'agit d'assurer que les données ne soient pas modifiées indûment,
que ce soit accidentellement ou volontairement.

=== Confidentialité ===

Il est imperatif d'assurer la _confidentialité_ des données en fonction de leur
_impact de publication_. Deux types d'informations sont souvent distingués:
* Les données *très sensibles*, dont la divulgation ou corruption menace le
  fonctionnement de la structure;
* Les données *réglementées*, qu'il est légalement requis de protéger, comme
  dans le cadre du RGPD.

=== Traçabilité ===

Les accès et modifications sur les données doivent être _tracés_ dans le temps,
assurant un meilleur diagnostique en cas d'incident, et la surveillance des
actions illicites.

== Qualités ==

=== Elasticité ===

L'_élasticité_ ou _scalabilité_ assure une capacité d'adaptation aux contraintes
et demandes nouvelles auquels peut faire face le système. On la divise en deux
catégorie:
* L'élasticité *verticale* permet l'ajout d'éléments nouveaux au système;
* L'élasticité *horizontale* module le nombre d'éléments déjà présents.

=== Couplage lâche ===

Le _couplage_ caractérise l'_adhérence_ et la _dépendance_ des éléments entre
eux. Le  _couplage lâche_ ou _découplage_ assure une l'indépendance envers les
éléments *internes* (services) et *externes*. 

=== Simplicité ===

La _simplicité_ du système assurera qu'il sera facilement appréhensible,
résilient du fait du faible nombre d'éléments, et extensible.
