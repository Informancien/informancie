= Contrôle des systèmes de fichiers GNU/Linux =

Les utilitaires système GNU/Linux `df` et `du` (voir *df(1)* et *du(1)*),
hérités d'Unix, permettent de contrôler l'utilisation des systèmes de fichiers:
`df` fourni un résumé tandis que `du` décomposé l'utilisation par fichier.

== Statistiques d'utilisation avec df ==

`df [<fs> ...]` (Disk Free) sort les statistiques d'utilisation des systèmes de
fichiers. Sans argument, tous les systèmes de fichiers *montés* sont listés,
sinon ceux dans les partitions ou aux points de montages `<fs> ...`. Par
défaut, `df` affiche les taux de blocs de 1Kio libres et utilisés. Ses
paramètres indiquent les informations à afficher:
* `-B<size>` Exprimer les statistiques en blocs de taille `<size>` au lieu de 
  1Kio;
* `-m` Exprimer les statistiques en Mibioctets;
* `-h` Exprimer les statistiques en unités binaires, puissances de 2 (Human
  readable);
* `-H` Exprimer les statistiques en unités du Système International
  (SI), puissances de 10;
* `-i` Afficher les statistiques d'utilisation des Inœuds;
* `-T` Afficher aussi le Type de système de fichiers;
* `--sync` Vidanger le tampon des écritures avant d'afficher les statistiques.

Il faut bien noter la différence entre `-h` et `-H`: `-h` affichera les
statistiques en multiples de kibioctet (Kio), c'est-à-dire $2^10$ octets, alors
que `-H` le fera en multiples de kilooctet (Ko), unité du Système International
qui elle correspond à $10^3$ octets.

Si des opérations d'écriture importantes sont en attentes dans le tampon du
noyau, les statistiques pourront très vite devenir obsolètes après leur
affichage. L'option `--sync` demande la réalisation des écritures en attente
avant que `df` n'obtienne les statistiques, afin d'assurer leur validité.

== Utilisation dans l'arborescence avec du ==

`du [<file> ...]` (Disk Usage) parcourt récursivement les dossiers à partir du
courant, en affichant chaque fichier accompagné de son occupation disque
exprimée en blocs de 1Kio. Si les arguments `<files> ...` sont passés, ces
fichiers ou dossiers seront parcourus et listés à la place du répertoire
courant. Le poids total des fichiers d'un dossier est aussi affiché après que
tous ses enfants aient étés parcourus. Ses paramètres définissent la sortie et
le comportement de la commande:
* `-B<size>` Exprimer les statistiques en blocs de taille `<size>` au lieu de 
  1Kio;
* `-m` Exprimer les statistiques en Mibioctets;
* `-h` Exprimer les statistiques en unités binaires, puissances de 2 (Human
  readable);
* `-c` Afficher les total des poids listés (Count);
* `-s` N'afficher que le poids total des dossiers parcourus (Summarize);
* `-L` Déreférencer, c'est-à-dire suivre les suivre, les Liens symboliques;
* `-x` Ne pas parcourir les points de montage des autres systèmes de fichiers.

Il faut bien comprendre que par défaut, `du` parcourt les dossier
récursivement, *quels que soit leur système de fichier réel*, et suivra donc
les points de montages vers d'autres systèmes de fichiers. Ce comportement
n'est pas souhaité si l'on veut étudier l'utilisation de l'espace sur un unique
système de fichiers. Dans ce dernier cas, on passera l'option `-x`, qui indique
que le parcourt doit se limiter aux systèmes de fichiers des dossiers initiaux.

== Confusion entre df et du ==

Le nom `df` étant l'abréviation de Disk Free, cela semble indiquer que cette
commande ne sort que l'espace libre sur un disque, en opposition à `du`, Disk
Usage, qui donne l'utilisation d'un disque par fichier. Or, il est totalement
faux que `df` n'affiche que la capacité libre: *il donne aussi celle utilisée*,
mais pour l'intégralité d'un système de fichiers, et non décomposée par
fichier, comme le fait `du`.

Pour éviter cette confusion, on peut utiliser le nom mnémonique Display
Filesystem (en français Afficher le Système de fichiers) pour la commande `df`,
qui semble moins ambigües que le nom d'origine Disk Free.
