= Nettoyant écologique pour écran =

Cette page explique la fabrication, très facile, d'une solution nettoyante
pour écrans, écologique et totalement biodégradable. Elle nettoie efficacement:
* Les écrans plats, mats ou brillants;
* Les écrans cathodiques;
* L'extérieur des châssis d'ordinateurs et des périphériques;
* Les plastiques, le verre et les métaux *inoxydables*, lisses ou avec une
  légère rugosité régulière (brossés ou sablés).

Un écran sale est particulièrement dérangeant, et peut même devenir illisible à
cause de taches réflechissants la lumière. Mais c'est une surface
particulièrement délicate, et les traditionnels produits d'entretient ne
retirent souvent pas les salissures, et ne font parfois que les étaler.

Un marché à donc était créé pour vendre des produits de nettoyage d'écrans,
ceux-ci sont souvent importés, onéreux, inefficaces, contiennent des susbtances
poluantes et des additifs inutiles tels des parfums. C'est par exemple le cas
du vaporisateur Think Xtra, vendu entre 10 et 15€ en conditionnement de 250ml
(à cette date, août 2021).

Certains nettoyants pour écrans vendus comme écologiques se révelent quant à
eux étonnament efficaces. Reste les problèmes d'importation, des additifs 
d'utilités discutables (souvent pour épaissir la solution), des emballages et
conditionnements (chaque dose livrée dans un carton imprimé, conditionnée dans
un vaporisateur en plastique). De plus, pour rester dans les principes des
basses technologies, il faut faire au plus simple et rester sobre, et donc
utiliser le strict minimum de substances.

En se basant sur les fiches de données de sécurité (Security Data Sheets en
anglais) des produits dit écologiques, on note deux ingrédients principaux:
* L'eau distillé, que l'on peut substituer par de l'eau du robinet;
* Le citrate de sodium, mélange de soude caustique et d'acide citrique.

Il s'agit donc d'une simple solution aqueuse de citrate de sodium: elle est
totalement biodégradable, et n'est ni toxique, ni irritante. 

== Préparation du citrate de sodium ==

Le _citrate de sodium_ est commercialisé sous forme de poudre, mais peut
extrêmement facilement être obtenu à partir d'un mélange de jus de citron et de
bicarbonate de soude:
1) Dans un grand récipient, verser le jus de citron *filtré*, il doit être 
   *sans aucune pulpe* (un coupon 100% coton tissé fin marche parfaitement comme
   filtre);
2) Saupoudrer de bicarbonate de soude, le mélange va immédiatement mousser.
   Continuer à en ajouter doucement *jusqu'à saturation*, c'est-à-dire que le
   bicarbonate ne se dissolve plus;
3) Jeter le liquide, reserver la mousse et le dépôt et les laisser sécher
   complétement à l'air libre, de préférence dans un endroit sec et ensoleillé
   (le bicardonate absorbe l'humidité de l'air).

== Prépration du nettoyant ==

Une fois le citrate de sodium sec, on prépare le nettoyant:
1) Dans un bol, diluer du citrate de sodium à une concentration d'environ 5%
   dans de l'eau du robinet;
2) Mélanger vigoureusement jusqu'à dissolution totale du citrate de sodium. Si
   de la poudre ne se dilue pas, ajouter de l'eau;
3) Pour éliminer les cristaux de bicarbonate qui pourraient rayer l'écran,
   passer le mélange dans un filtre fin (un coupon de tissus 100% coton fait
   toujours un parfait filtre). Répéter plusieurs fois si nécessaire;
4) Transvaser la solution filtrée dans une bouteille pour le stockage.

Au besoin, l'eau du robinet peut être substitué par de l'eau distillée.

TODO Conservation

TODO Photos

== Nettoyage d'un écran ==

La solution réalisée ici est particulièrement efficace pour retirer les taches
d'un écran, et peut même être utilisée sur l'exterieur des ordinateurs et
périphériques.

Quelques considérations Avant utilisation du nettoyant:
* Il est *totalement liquide* contrairement à ceux commercialisés, il ne faut
  donc pas asperger l'appareil, l'eau pourrait couler à l'intérieur et causer
  des dommages;
* S'il n'est pas essuyé, il *laisse des traces blanches*. Celles-ci ne sont pas
  indélébiles, mais doivent être à nouveau nettoyées;
* Un peu de pression est requise pour éliminer les taches récalcitrantes de
  l'écran, il faut faire attention à ne pas user de trop de force, sous peine
  de créer une *marque de pression permanentes* ou de *rayer la dalle*. Vous
  êtes le seul juge de la force requise pour nettoyer vos équipements sans les
  abîmer.

Pour appliquer le produit sur un écran, on procédera ainsi:
1) Eteindre l'écran;
2) Imbiber un chiffon doux et épais, préférablement non-pelucheux (une
   chaussette propre en coton par exemple), et passer délicatement sur la
   surface en zigzag sur toute la largeur. Appuyer *modérément* sur les taches
   resistantes. L'épaisseur du chiffon assure le dépôt d'une quantité
   convenable de produit, s'il n'y en a pas suffisament, il séchera trop vite
   et laissera des traces blanches;
3) Essuyer immédiatement le produit jusqu'à totale évaporation avec un chiffon
   sec du même type que le précédent;
4) Répéter au besoin, afin d'éliminer toutes les taches et traces blanches
   potentiellement laissées par le produit ayant séché trop vite.

L'écran redevient propre et sans marques des passages du chiffon, contrairement
au résultat obtenu avec nombre de produits.

TODO Photos

== Surfaces testées ==

Ci-dessous un tableau des surfaces essayées avec ce produit. L'application a 
été faite avec la méthode décrite dans la section précédente.

| Surface                                         | Fréquence ou nombre de nettoyages | Durée     | Résultat                                                                      |
|-------------------------------------------------|-----------------------------------|-----------|-------------------------------------------------------------------------------|
| Dalle LCD brillant                              | 1 par semaine                     | 1 mois    |                                                                               |
| Affichage LCD monochrome                        | 2 tests                           | >         | Tests concluants, aucune trace ni marque.                                     |
| Verres plastiques anti-lumière bleu de lunettes | 1 par jour                        | 1 semaine |                                                                               |
| Verres anti-lumière bleu de lunettes            | 1 par jour                        | 1 semaine |                                                                               |
| Lentille de loupe en verre                      | Unique                            | >         | Test concluant, brillance retrouvée, aucune trace ni marque.                  |
| Verre trempé                                    | Unique                            | >         |                                                                               |
| Plastique moulé                                 | 2 tests                           | >         | Tests concluants, aucune trace ni marque.                                     |
| Aluminium brossé                                | Unique                            | >         | Pas de marques, très légères traces, bien imbiber le chiffon et vite essuyer. |

TODO Finir tests

== Liens externes ==

* [[https://ecomoist.co.uk/products-msds/]]
* [[https://fr.vikidia.org/wiki/Citrate_de_sodium]]
