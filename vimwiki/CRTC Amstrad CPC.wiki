= CRTC Amstrad CPC =

* [[file:files/docs/HD6845R-HD6845RS-CRTC.datasheet.pdf|Datasheet CRTC HD6845R/HD6845S (t0)]]
* [[file:files/docs/HD6845R-HD6845RS-CRTC.datasheet.anomalous.pdf|Datasheet CRTC HD6845R/HD6845S with anomalous (t0)]]
* [[file:files/docs/HD6845R-HD6845RS-CRTC.datasheet.anomalous.pdf|Datasheet CRTC UM6845R (t1)]]
* [[file:files/docs/MC6845-CRTC.datasheet.pdf|Datasheet CRTC MC6845 (t2)]]
* [[file:files/docs/ACCC1.7-FR.pdf|CRTC Comprendium 1.7 - Logon]]

== Généralités ==

La fonction du CRTC est de créer l'écran et de générer les signaux nécessaires à
son affichage: HSYNC, VSYNC, DISPEN et l'octet de la VRAM en cours sont envoyés
régulièrement au Gate Array.

Le CRTC gère également un stylo optique, le mode interlace et un curseur texte.
Sur un CPC, son horloge 1Mhz est fourni par le Gate Array.

Le CRTC possède 19 registres. Le premier (AR) sert à sélectionner lequel des
autres registres (Rx) sera concerné par la prochaine commande qui lui est
adressée.

Les registres R0-R9 sont des registres de timing.
* R0-R3 gèrent l'horizontale. Avant d'entrer dans les détails, on notera :
  * R2 déclenche le HSYNC (HBL);
  * R3 = 0 désactive la HBL;
  * R0 - R1 = temps de retour du faisceau sur la gauche.
* R4-R9 gèrent la verticale;
  * R4-R5 permettent de fixer 50Hz ou 60Hz;
  * R7 déclenche le VSYNC (VBL).
* R10-R11/R14-R15 gère le curseur texte (non utilisé sur CPC);
* R12-R13 pointe l'adresse de début de la VRAM;
* R16-R17 gère le stylo optique.
 
Les relations d'ordre impératives sont les suivantes :
* R0 > R1;
* R2 > R1;
* R6 < R4;
* R7 <= R4, R7 >= R6
* (R4+1)*(R9+1)+R5=312.

== Sommaire ==

* [[Cablage et synchronisation du CRTC]]
* [[Adressage, Compteurs et Overflow du CRTC]]
* [[Description des registres du CRTC]]
* [[Adressage de la VRAM]]
* [[Stabilité du CRTC en fréquence]]
* [[Algorithmes du CRTC]]
* [[Synchronisation et modifications des registres du CRTC]]
* [[Exploitations du CRTC]]
* [[Types de CRTC et compatibilité]]

TODO https://neuro-sys.github.io/2019/10/01/amstrad-cpc-crtc.html
