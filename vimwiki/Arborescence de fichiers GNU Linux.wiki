= Arborescence de fichiers GNU/Linux =

Aujourd'hui, la grande majorité des distributions GNU/Linux tentent d'adhèrer au
_Filesystem Hierarchy Standard_ (abrégé _FHS_, en français la Standard de
Hiérarchie du Système de fichiers, voir *file-hierarchy(7)*), convention sur
l'arborescence des dossiers à la racine (root en anglais) du système de fichier,
au point de montage `/`.

Mais cette arborescence trouve son origine dans les années 1970, ou sa nécessité
était un effet secondaire de l'espace de stockage limité: celle-ci est née de la
contrainte, et non de considérations raisonnées. Le rôle exact de chaque
dossier de cette arborescence n'a donc était rationalisé qu'a posteriori de leur
existence. Ceci engendre encore jusqu'à nos jours des incohérences, redondances,
imcompréhensions, et interprétations différentes d'une distribution à l'autre.

C'est dû à ce long héritage jamais abandonné, maintenant profondément enraciné
dans la culture et la technique, que le FHS même présente lui même des
incohérences, et qu'aucune règle universelle n'est observable.

On peut tout de même, par l'étude du FHS, de l'héritage, des conventions et
implémentations, tenter de tirer un portrait de la forme que prend généralement
l'arborescence racine des distributions GNU/Linux:
* `/bin/` BINaires essentiels au système, exécutables par les utilisateurs;
* `/sbin/` Binaires d'administration nécessitant le plus souvent les
  droits de `root` (System BINaries);
* `/boot/` Fichiers necéssaires à l'amorçage dont le noyau et l'initrd;
* `/dev/` Fichiers périphériques (DEVices an anglais);
* `/etc/` Fichiers de configuration du système;
* `/root/` Dossier de l'utilisateur `root`;
* `/home/` Dossiers des utilisateurs ormis `root`;
* `/lib/` LIBrairies requises par les binaires de `/bin/` et `/sbin/`;
* `/lib<arch>/` Equivalent de `/lib/` mais contenant les librairies compilées
  pour l'architecture étrangère `<arch>`. Souvent utilisé sur les systèmes
  supportants plusieurs jeux d'instructions, comme le x86-64;
* `/mnt/` Points de MoNTages temporaires, pour les systèmes de fichiers qui ne
  sont pas mentionnés dans le fichier `/etc/fstab`;
* `/media/` Points de montages dédiés aux médias amovibles (clé USB, CD...);
TODO * `/opt/` Programmes additionnels tiers (OPTionnels), souvent ceux non
  empaquetés spécifiquement pour la distribution;
* `/proc/` Pseudo système de fichiers offrant des informations sur le noyau;
TODO * `/run/`
TODO * `/sys/`
* `/var/` Données régulièrement altérées (VARiables) sans l'intervention
  directe d'un utilisateur (files d'attentes, bases de données, journaux...);
* `/srv/` Fichiers délivrés (SeRVed, servis en anglais) par des services de
  l'hôte (via FTP, HTTP...);
* `/tmp/` Dossier de stockage TeMPoraire utilisable par *tous les utilisateurs*;
* `/usr/` Programmes en espace utilisateur, librairies et ressources fournis par
  la distribution GNU/Linux utilisée.

TODO == Répértoire de l'administrateur /root/ ==

root = utilisateur gérant la racine
historiquement objets personnels de l'admin directement à la racine, linux
ajoute /root/ pour ranger

TODO == Différence entre /bin/ et /sbin/ ==

TODO == /opt/ et programmes additionnels ==

== Arborescence /usr/ ==

Aux origines d'Unix, `/usr/` contenait les dossiers des utilisateurs (son nom
est la contraction de l'anglais user), comme le fait actuellement `/home/`. Il
sert aujourd'hui de répertoire d'installation pour les programmes en espace
utilisateur ainsi que leurs librairies et autres ressources. De par cette
nouvelle utilisation, est parfois donné au répertoire `/usr/` le retroacronyme
_User System Ressources_.

Le contenu des sous-répertoires d'`/usr/` est le plus couramment fournis par la
distribution GNU/Linux utilisée, et maintenu via son gestionnaire de paquet.
Seul `/usr/local/` fait exception à cette règle:
* `/usr/bin/` Principal répertoire des programmes utilisateurs de la
  distribution;
* `/usr/sbin/` Programmes d'administration requierant les droits de `root` pour
  s'exécuter;
* `/usr/lib/` Librairies liées dynamiquement par les programmes utilisateurs;
* `/usr/share/` Ressources des programmes indépendantes de l'achitecture, tels
  les images ou la documentation;
* `/usr/include/` Fichiers d'entête (d'extension `.h`) pour la compilation des
  programmes utilisateurs écrit en C et C++;
* `/usr/src/` Sources fournies par la distribution GNU/Linux courante;
* `/usr/local/` Contient une arborescence structurée identiquement à `/usr/`,
  mais celle-ci est réservée pour les programmes, librairies, ressources et
  sources fournies *par l'administrateur, et non la distribution*. Il s'agit
  souvent de programmes respectant la répartition Unix des fichiers (exécutables
  dans `bin/`, librairies dans `lib/`...), mais non fournis par la distribution.

TODO == Répartition des binaires ==

* `/bin/`
* `/sbin/`
* `/usr/bin/`
* `/usr/sbin/`
* `/usr/local` Compilés localement, partition dédiée pour réinstallation
* `/opt/`

TODO == Données dans /var/ ==

TODO == Fichiers temporaires de /tmp/ ==

== Liens externes ==

* [[https://refspecs.linuxfoundation.org/FHS_2.3/fhs-2.3.html]]
* [[http://lists.busybox.net/pipermail/busybox/2010-December/074114.html]]
* [[https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard]]
* [[https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/usr.html]]
* [[https://askubuntu.com/questions/130186/what-is-the-rationale-for-the-usr-directory]]
