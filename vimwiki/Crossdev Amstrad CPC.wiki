= Crossdev Amstrad CPC =

Seront listés ici des outils fonctionnant sous linux et entrant dans le flux
de cross-dev linux/CPC.

== Emulateurs ==

Tous les émulateurs:
https://cpcrulez.fr/emulateurs_download-platform-win-linux-macos.htm

En ligne:
* [[https://www.retrovm.com/|CPCBox]]
	* non opensource, non libre

* [[http://www.retrovirtualmachine.org/en/|RetroVM]]
	* tests:
		* soucis avec les anciennes démos
		* comportement selon type CRTC ?
		* différences avec la synchro GA
	* non opensource, non libre
	* lastdev: 10/07/2019
	* linux, mac, windows

* [[https://github.com/cpcitor/cpcec|cpcec]], CNGSoft
	* tests:
		* debugger rudimentaire
		* bon pour les demos
		* bon pour les synchros GA
	* opensource, GPLv3
	* [[http://cpcrulez.fr/forum/viewtopic.php?t=6195|changelog CPC Rulez]]
	* lastdev: 22/04/21
	* linux, windows
	* compilation: binaire fourni pour windows
		* récupérer la [[https://github.com/libsdl-org/SDL/releases/tag/release-2.28.5|SDL2-dev]]
		* la compiler ou installer les paquets libsdl2-dev et libsdl2-2.0-0
		{{{bash
			$ ./configure
			$ make all
			# make install
		}}}
		* passer les noms de fichiers en minuscules:
		{{{bash
			$ for i in *; do mv "$i" "$(echo $i | tr A-Z a-z)"; done
		}}}
		* compiler cpcec:
		{{{bash
			$ cc cpcec.c -o cpcec -lSDL2
		}}}

* [[https://gitlab.com/norecess464/cpcec-plus|cpcec-plus]], fork de norecess depuis cpcec/CNGSoft
	* testé:
		* peu de démos passent
		* bon pour les synchros GA
	* opensource, GPLv3 par héritage
	* linux only
	* lastdev: 06/08/22
	* compilation:
	{{{bash
		$ make release
	}}}

* [[https://bitbucket.org/norecess464/cpcec-gtk/src/master/|cpcec-gtk]]
	* "cngsoft"/Nicolas-Gonzalez, Arnaud "norecess"
	* opensource
	* linux only
	* last commit: 2022/07
	* compilation:
	{{{bash
		# apt install gtk+3.0 libsdl2-2.0-0 libgtk-3-0
		# make
	}}}
	* ou [[https://bitbucket.org/norecess464/cpcec-gtk/src/master/cpcec-gtk.deb|package]]

* CPCEPower, Megachur
	* [[http://www.cpcwiki.eu/forum/emulators/cpcepower-v1911/|CPCEPower v1911]], lastdev: 24/11/19
	* [[https://www.cpcwiki.eu/forum/emulators/cpcepower-v2004/|CPCEPower v2004]], lastdev: 20/04/20
	* [[https://www.cpcwiki.eu/forum/emulators/cpcepower-v2011/|CPCEPower v2011]], lastdev: 10/01/21
	* opensource ?
	* linux, windows
	* compilation: binaires fournis

* [[https://github.com/Tom1975/SugarboxV2|SugarBox]], Lone
	* opensource, licence non précisée
	* lastdev: 17/11/21
	* linux, windows

* [[https://bitbucket.org/ponceto/xcpc/src/master/|xcpc]], Olivier Poncet
	* opensource, GPLv2
	* en cours de dev
	* linux

* [[https://gitlab.com/norecess464/cpcec-gtk/-/releases|cpcec-gtk]], norecess
	* opensource
	* linux only

* [[https://www.cpcwiki.eu/forum/emulators/amspirit-a-new-cpc-emulator-for-windows/|amspirit]]
 	* https://www.amspirit.fr, https://forum.system-cfg.com/viewtopic.php?t=11535
	* "free software", code non diffusé
	* donne [[https://shaker.logonsystem.fr/tests|les meilleurs résultats]] au Shaker/Logon
	* lastdev: 04/2023
	* windows 10, 64b & 32b, version linux prévue, tourne sous wine
	* nécessite l'installation de Microsoft Visual C++ 2015 Redistributable Update 3 RC
		https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170#visual-studio-2015-2017-2019-and-2022

* [[https://github.com/ColinPitrat/caprice32|caprice32]]
	* opensource
	* linux

* [[http://www.roudoudou.com/ACE-DL/|Ace]]
	* Original emulator by [[http://ace.cpcscene.net/fr:introduction|Offset]]
	* Linux/MacOS/Windows

== Compilateurs ==

* [[https://gcc.godbolt.org/|Compiler Explorer]]
  * compilateurs en ligne avec sortie texte du code compilé
  * Z80: compilateur SDCC 4.0.0, option -mz80
 
* [[https://ugbasic.iwashere.eu/|ugBASIC]]
  * compilateur BASIC
  * multiplateforme
  * IDE: Windows only
  * compilateur: [[https://ugbasic.iwashere.eu/install|Linux]], Windows
  * licence Apache 2.0

== Assembleur ==

* [[https://github.com/EdouardBERGE/rasm|rasm]], roudoudou
  * opensource
  * linux, windows
  * compilation
  {{{bash
    $ make
  }}}

* [[http://orgams.wikidot.com/|orgams]], ast
	* non opensource
	* real hardware, avec extension [[https://www.octoate.de/2014/04/28/x-mem-a-new-memory-expansion/|X-MEM]]
	  ou [[https://www.octoate.de/2016/05/11/wifi-hardware-expansion-for-the-amstrad-cpc/|Amstrad Wifi]], version ROM uniquement
  
== Disquette / K7 ==

* [[https://github.com/Tom1975/SugarConvTape|SugarConvTape]], Lone
  * opensource
  * linux, windows
  * compilation: binaires fournis
* [[https://github.com/Tom1975/SugarConvDsk|SugarConvDsk]], Lone
  * opensource
  * linux, windows
  * compilation:
	{{{bash
		$ git clone https://github.com/Tom1975/SugarConvDsk.git
		$ cd googletest
		$ git clone https://github.com/google/googletest.git
		$ cd .. && rm -r zlib
		$ git clone https://github.com/madler/zlib.git
		$ cmake .
		$ make
		-> SugarConvDsk/
	}}}

== Ecran, sprites ==

* [[https://github.com/EdouardBERGE/convgeneric.git|convgeneric]], roudoudou
  * opensource
  * linux, windows
  * compilation
  {{{bash
    $ cc -lm -lrt -lpng16 -march=native convgeneric.c -o convgeneric
  }}}
  * exemples, sur file.png ("image pour le web"):
  {{{bash
  	# écran en overscan
  	$ convgeneric file.png -m 1 -scr -lb 8 -w 93 -g -ls 136
  	-m: mode graphique cpc
  	-scr: screen cpc
  	-lb: nb de scanlines par bloc
  	-w: nb d'octets par ligne
  	-g: palette triée
  	-ls: quand on a 2 écrans, nb de lignes du 1er écran 
	# sprites depuis png (sprites alignés verticalement dans le png)
	$ congeneric file.png -hsp -c 5 -split 256
	-hsp: sprites hard
	-c 5: nb de sprites maximum à extraire
	-split: taille des fichiers sprites (code pour la taille), en octets
  }}}
  
* [[https://github.com/jeromelesaux/martine|martine]], Jérôme Lesaux
  * opensource
  * linux, windows
  * [[https://amstradplus.forumforever.com/t513-martine-fait-du-dessin.htm?start=45|updates]]
  * [[https://github.com/jeromelesaux/martine/releases|binaires fournis]]
  * sinon, compilation:
		* installer go
		{{{bash
		$ wget https://dl.google.com/go/go1.13.linux-amd64.tar.gz
		$ sudo tar -C /usr/local/src/ -xzf go1.13.linux-amd64.tar.gz
		$ export GOPATH=/usr/local/src/go/
		$ export GO111MODULE=on
		}}}
		* patcher le package martine
		{{{bash
		[go/pkg/mod/github.com/jeromelesaux/martine@v0.0.0-20210422195327-00e7e655e307/export/file/dsk.go]
		40   var floppy *dsk.DSK
		41   if exportType.ExtendedDsk {
		42     floppy = dsk.FormatDsk(10, 80, 1, 1, 0)
		43   } else {
		44     floppy = dsk.FormatDsk(9, 40, 1, 0, 0)
		45   }
		}}}
		* compiler martine
		{{{bash
		$ make build
		}}}
  * exemple:
  		{{{bash
		$ martine -i 16x16-ff-selected.png -m 1
		}}}
  
* [[https://github.com/EdouardBERGE/hspcompiler|compiler]], roudoudou
	* opensource
	* linux, windows
	* compilation:
	* exemples
	{{{bash
		$ compiler -d phase1.bin phase2.bin > phase0102.asm
		$ compiler -d phase2.bin phase3.bin > phase0203.asm
	}}}

* [[http://tilestudio.sourceforge.net/|Tiles Studio]], map editor
	* opensource, licence MIT
	* windows only

* [[http://www.pouet.net/prod.php?which=67770|Perfect Pix]], Rhino
	* not opensource

* [[http://www.pouet.net/prod.php?which=88808|UniPixelViewer]], Devilmarkus
	* non opensource
	* windows

* [[https://galsas.github.io/amstrad-cpc-sprite-creator/|CPC Sprite Creator]], galsas
	* opensource
	* online

== IDE ==

* [[https://lemonspawn.com/turbo-rascal-syntax-error-expected-but-begin/|TRSE]]
	* IDE, compiler, programming language, resource editor
	* Windows 64-bit, Linux 64-bit and OS X
	* several computers supported
	* free but not opensource

== Divers ==

* [[https://github.com/antoniovillena/ticks|ticks]], Antonio Villena
	* executes instructions and counts the time that has elapsed
	* opensource

* [[https://github.com/cpcsdk|CPC-SDK]]
	* cross-dev tools
	* z80 vim configuration
	* iDSK: DSK edit
		{{{bash
			$ git clone https://github.com/cpcsdk/idsk.git
			# cmake .
			# make
		}}}

* [[https://colourclash.co.uk/cpc-analyser/|CPC Analyser]]
	* reverse engineering
	* Windows 10, Linux
	* opensource, licence MIT
	* [[https://www.cpcwiki.eu/forum/programming/any-interest-in-a-cpc-reverse-engineering-tool|infos]]
	* [[https://github.com/TheGoodDoktor/8BitAnalysers|github]]
	* build: appremment ubuntu-latest seulement
	  [[https://github.com/TheGoodDoktor/8BitAnalysers/actions|Build CI]]

== Spécifique linux ==
  
* Scene 3d: [[https://cloud.blender.org/p/blender-inside-out/560414b7044a2a00c4a6da98|blender]]

= Spécifique windows =

* Animation: [[https://graphicsgale.com/us/|Graphics Gale]]
