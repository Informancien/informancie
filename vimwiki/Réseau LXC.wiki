= Réseau LXC =

La communication réseau des conteneurs entre eux, entre les conteneurs et
l'hôte et des conteneurs vers d'autres réseaux néccéssite la mise en place d'une
couche réseau niveau liaison (couche 2 selon le modèle OSI).

== Bridgé ==

Dans le ces d'un conteneur non privilégié, l'utilisateur doit pouvoir configurer
le bridge via
[[Conteneur LXC privilégié et non-privilégié#lxc-user-nic|lxc-user-nic]].

=== Bridge partagé avec l'hôte ===

Si les conteneurs doivent être sur le même réseau que l'hôte, on peut créer un
_bridge partagé avec l'hôte_. Une interface de l'hôte est incluse dans un
bridge, auxquels sont connéctés les conteneurs via une paire de veth.

=== Bridge indépendant ===

Si les conteneurs doivent être sur un réseau différent de l'hôte, on doit leur
dédier un _bridge indépendant_. Le service lxc-net, souvent fourni avec les
diverses distributions de LXC, se charge de la création du bridge, et de la
configuration des conteneurs.

==== Service lxc-net ====

`lxc-net` est un service fourni avec LXC permettant de créer et maintenir un
bridge indépendant, dédiant un réseau aux conteneurs. Pour ce faire, il:
* Active le forwarding sur l'hôte;
* Instancie un processus `dnsmasq` chargé d'attribuer leurs adresses IP aux
  conteneurs via DHCP;
* Configure `iptables` pour masquer les paquets sur la table `nat` via la cible
  `MASQUERADE` pour accéder aux réseaux externes.

===== Configuration =====

Les fichiers `/etc/default/lxc` et `/etc/default/lxc-net` sont sourcés depuis
le script Bash du service. Les variables suivantes peuvent être définies:
* `USE_LXC_BRIDGE` Lance le service avec `true`;
* `LXC_BRIDGE` Nom du bridge;
* `LXC_BRIDGE_MAC` Adresse MAC du bridge;
* `LXC_ADDR` Adresse du bridge et d'écoute `dnsmasq`;
* `LXC_NETMASK` Masque réseau du bridge;
* `LXC_NETWORK` Réseau des conteneurs;
* `LXC_DHCP_RANGE` Plage DHCP;
* `LXC_DHCP_MAX` Baux DHCP maximum;
* `LXC_DHCP_CONFILE` Fichier de configuration de `dnsmasq`;
* `LXC_DOMAIN` Domaine du serveur DHCP;
* `LXC_IPV6_ADDR`, `LXC_IPV6_MASK`, `LXC_IPV6_NETWORK`, `LXC_IPV6_NAT`
  Configuration IPv6, analogue à celle IPv4.

===== Conflit avec le service dnsmasq =====

Si `dnsmasq` est lancé sur l'hôte des conteneurs, il est possible que le
lancement de `lxc-net` plante avec le message "Adresse IP déjà utilisée". Par
défaut, `dnsmasq` écoute sur toutes les interfaces via la pseudo-interface `*`:
lorsque `lxc-net` tente de lancer sa propre instance de `dnsmasq` exclusivement
pour son bridge, il y a conflit, le premier lancé écoute aussi sur le bridge
dès sa création. Pour éviter se comportement, le service `dnsmasq` du système
doit avoir les options suivantes (souvent le fichier de configuration
`/etc/dnsmasq.conf`):
{{{conf
# N'utilise pas la pseudo-interface *, s'attache à chacune des interfaces 
bind-interfaces
# Ignorer le bridge, doit être la valeur de LXC_BRIDGE, par défaut lxcbr0
except-interface=lxcbr0
}}}

Sous LXC 2.0 et les versions précédentes, il faut aussi bien définir la
variable de configuration `LXC_DHCP_CONFILE` de `lxc-net` vers le chemin d'un
autre fichier de configuration que celui édité précédemment. Le fichier en lui
même peut être vide, ou bien `/dev/null`, mais la variable doit
*obligatoirement avoir comme valeur une chaîne non vide*, sinon, `lxc-net`
réutilisera le chemin par défaut de la configuration de `dnsmasq`.
