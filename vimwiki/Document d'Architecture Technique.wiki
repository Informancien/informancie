= Document d'Architecture Technique =

Le _document d'architecture technique_ documente le plan de mise en œuvre de
l'architecture, il:
* Synthétise les objectifs portés par l'architecture et ses differentes
  [[Architecture des systèmes d'information#Couches|couches]];
* Etabli les contraintes et engagements de services à respecter (PDMA,
  disponibilité...);
* Explicite les réponses aux engagements apportées par l'achitecture;
* Défini le plan de réalisation du projet;
* Offre un support de communication au projet.

== Plan ==

1. Besoins fonctionnels;
	* Fonctions du systèmes et les attentes;
	* Historique du projet;
	* Contraintes métiers;
	* Acteurs.
2. Besoins non-fonctionnels (contraintes techniques...);
3. Schémas des [[Architecture des systèmes d'information#Couches|couches]];
	* [[Couche fonctionnelle|Fonctionnelle]];
	* [[Couche applicative|Applicative]];
		- Matrice des flux (source, destination, protocole, réseau).
	* [[Couche infrastructure|Infrastructure]];
	* [[Couche opérationnelle|Opérationnelle]].
		- Services souscrits (serveurs mutualisés...).
4. Décisions d'achitecture (avec pour chacune le détail);
	* Intitulé;
	* Contexte et problématique;
	* Options considérées;
	* Décision et justification.
5. Plan de réalisation;
	* Matrice RACI;
	* Calendrier (tâches parrallèles ou séquentielles).
6. Risques et mitigations possibles;
7. Coûts.
