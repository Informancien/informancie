= Syntaxe SQL =

La syntaxe SQL est définie par le standard ISO/IEC 9075, cette page n'est pas 
exhautive et ne couvre que les fonctions courantes. A noter que la très grande
majorités de ses implémentations dérogent au standard, ajoutent des extensions,
remplacent certaines fonctionnalitées ou ne les fournissent pas.

Une _instruction_ (statement) SQL est divisé en _clauses_, et se termine
*toujours* par `;`. Les clauses contiennent:
* Des _mots clés_ écrits en capitales (exemple: `SELECT`);
* Des _identifiants_ faisants réferences à des objets de la base de données
  (table, colonne...);
* Des _expressions_ retournants des nombres ou des tables (exemple:
  `field + 1`);
* Des _prédicats_ définissant les critères de sélection des requêtes ou le
  contrôle d'exécution.

Une _requête_ (query) est une clause ouvrant une instruction, permettant
d'interagir avec la base de donnée, de la modifier ou de retourner des données
selon des critères de sélections.

Les instructions sont réparties en quatre sous-langages:
* Le _Data Query Language_ (DQL) pour la lecture des données;
* Le _Data Manipulation Language_ (DML) pour la manipulation des données;
* Le _Data Definition Language_ (DDL) pour définir les objets;
* Le _Data Control Language_ (DCL) pour le contrôle des accès aux données.

== Commentaire ==

{{{sql
-- Commentaire uniligne, obligatoirement suivi d'un retour à la ligne
/* Commentaire multiligne */
}}}

Les commentaires sont utilisés dans les attaques par injection SQL, il est donc
extrêmement important de les gérer proprement lors de la construction d'une 
instruction dans un programme.

== Identifiants ==

Un identifiant fait reférence à un objet simplement par son nom, qui ne peut
pas contenir d'espaces. Un même nom peut identifier plusieurs types d'objets,
celui voulu sera sélectionné en fonction du contexte. Par exemple, une table et
une colonne peuvent toutes deux porter le nom `obj`.

Dans un contexte ou plusieurs tables sont manipulées (c'est le cas avec la
clause `FROM` de la requête `SELECT`), la notation en `.` est utilisée pour
sélectionner une colonne d'une des tables:
{{{sql
<col> -- Colonne <col> de la table actuelle
<table>.<col> -- Colonne <col> de la table <table>
}}}

Une liste d'identifiants (`<tables>`, `<cols>` et `<vals>`) est faite en les
séparants par des virgules:
{{{sql
<id>[, ...] -- Liste d'identifiants
}}}

== Prédicat ==

| Opérateur                  | Fonction                                 |
|----------------------------|------------------------------------------|
| `=`                        | Egalité                                  |
| `<>`                       | Inégalité                                |
| `<`, `>`                   | Inférieur, supérieur                     |
| `<=`, `>=`                 | Inférieur/supérieur ou égale             |
| `NOT`                      | Inversion logique                        |
| `AND`, `OR`                | Et/ou logiques                           |
| `BETWEEN <x> AND <y>`      | Intervalle inclusif entre `<x>` et `<y>` |
| `LIKE <pattern>`           | Correspondance à `<pattern>`             |
| `IN tuple`                 | Appartenance à `<tuple>`                 |
| `IS NULL`                  | Egalité à `NULL`                         |
| `IS TRUE`, `IS FALSE`      | Comparaison booléenne                    |
| `<x> IS DISTINCT FROM <y>` | Inégalité, ou les deux sont `NULL`       |

Avec les opérateurs commencant par `IS`, `NOT` est placé après celui-ci, par
exemple `IS NOT NULL` ou `IS NOT DISTINCT FROM`.

= Types de données =

Le standard SQL laisse la définition de la majorité des types à
l'implémentation:
* `SMALLINT`, `INT`, `BIGINT` Entiers (implémentation libre);
* `{DECIMAL|NUMERIC}(<p>[, <s>])` Réél exact, de `<p>` nombres significatifs avec 
  `<s>` nombres décimaux;
* `FLOAT(<p>)` Réél approximatif de précision `<p>`;
* `REAL` et `DOUBLE PRECISION` Rééls approximatifs (implémentation libre);
* `CHAR(<s>)` Chaîne de caractère de taille *fixe* `<s>`;
* `VARCHAR(<s>)` Chaîne de caractère de taille *maximale* `<s>`;
* `CLOB(<s>[{K|M|G FROM|T|P}] [{CHARACTERS|OCTETS}])` Character large object, grande
  chaîne de caractères de taille maximale `<s>`;
* `BOOLEAN` Booléen;
* `BINARY(<s>)` Chaîne binaire de taille fixe `<s>`;
* `VARBINARY(<s>)` Chaîne binaire de taille maximale `<s>;
* `BLOB(<s>[{K|M|G|T|P}])` Binary large object de taille maximale `<s>`;
* `DATE` Année, mois et jours;
* `TIME` Heures, minutes et secondes;
* `TIMESTAMP` Horodatage.

== Data Query Language ==

La requête `SELECT` retourne une table de lignes sélectionnées dans les tables
voulues. *L'ordre des clause est immuable*:
{{{sql
SELECT [DISTINCT] {<cols>|*} /* Colonnes sélectionnées, DISTINCT supprime les
	doublons et * sélectionne toutes les colonnes */
	FROM <tables> -- Tables lues
		[JOIN <table> ON <predicate>] ... -- Jointure
	[WHERE <predicate>] -- Prédicat de sélection de ligne
	[GROUP BY <cols>] /* Colonnes de regroupement, les lignes ayants ces
		champs égaux sont fusionnées */
	[HAVING <predicate>] -- Prédicat sur le résultat de GROUP BY
	[ORDER BY <cols> [DESC]] -- Colonnes de tri, DESC inverse le tri
	[OFFSET <int>] -- Numéro de la première ligne sélectionnée
	[FETCH FIRST <int>]; -- Nombre de lignes sélectionnées
}}}

Une requête `SELECT` peut être imbriquée dans une autre en l'entourant avec
`(` et `)`.

=== Alias ===

Un identifiants de table ou de colonne des clauses `FROM` et `SELECT` peut être
temporairement renommé avec `AS`:
{{{sql
<id> AS <alias> -- L'utilisation d'<alias> fera référence à <id>
}}}

== Data Manipulation Language ==

=== Insertion d'une ligne ===

`INSERT` ajoute une ligne à la table spécifiée:
{{{sql
INSERT INTO <table>
	(<cols>)
	VALUES
	(<vals>)[,
	...];
}}}

=== Modification de lignes ===

`UPDATE` modifie les lignes sélectionnées:
{{{sql
UPDATE <table> FROM
	SET <col> = <val>[,
	...]
	[WHERE <predicate>];
}}}

=== Suppression de lignes ===

`DELETE` supprime les lignes sélectionnées:
{{{sql
DELETE FROM <table>
	[WHERE <predicate>];
}}}

=== Troncature d'une table ===

`TRUNCATE` vide une table, *toutes ses lignes sont supprimées*:
{{{sql
TRUNCATE TABLE <table>;
}}}

=== Fusion de tables ===

`MERGE` fusionne une table dans une autre:
{{{sql
MERGE INTO <table> USING <table> ON (<cols>)
	WHEN MATCHED THEN
		UPDATE SET <col> = <val>[, ...]
	WHEN NOT MATCHED THEN
		INSERT (<cols>) VALUES (<vals>);
}}}

Les lignes de la table `USING` sont importées dans `INTO`:
* Si les champs `ON` de la ligne de `USING` sont égaux au mêmes champs d'une
  ligne de `INTO`, alors `UPDATE` est appelé sur cette seconde ligne;
* Si les des champs `ON` d'une ligne de `USING` sont absents de `INTO`, alors
  `INSERT` est appelé sur cette seconde table.

== Data Definition Language ==

=== Création d'une table ===

`CREATE` créé un objet, le plus souvent une table. Les _contraintes_ définissent
les valeurs accéptées par les colonnes:
{{{sql
CREATE TABLE <table> (
	<col> <type> -- Nom et type de donnée de la colonne
 		[DEFAULT <val>] -- Valeur par défaut
		[NOT NULL] -- NULL interdit
		[{PRIMARY KEY -- Colonnes des clés primaires
		|UNIQUE}] -- Ne peut contenir deux fois la même valeur sauf NULL
		[REFERENCES <table> (<cols>)] /* Est une référence vers un
			champ UNIQUE ou PRIMARY KEY */
		[CHECK <predicate>][, -- Prédicat de vérification des valeurs
	...][, -- Répeter pour chaque colonne
	[{UNIQUE (<cols>), -- Colonnes uniques
	|PRIMARY KEY (<cols>),] -- Colonnes des clés primaires
	[FOREIGN KEY (<cols>) REFERENCES <table> (<cols>),] -- Référence
	[CHECK (<predicate>)]] -- Prédicat de vérification
);
}}}

=== Altération d'une table ===

`ALTER` modifie la structure d'une table:
{{{sql
ALTER TABLE <table> ADD <col definition>; -- Ajouter une colonne
ALTER TABLE <table> DROP <col>; -- Suppression d'une colonne
ALTER TABLE <table> ALTER <col> -- Modifier une colonne
	{SET DEFAULT <val> -- Valeur par défaut
	|DROP DEFAULT  -- Aucune valeur par défaut
	|SET NOT NULL -- NULL interdit
	|DROP NOT NULL -- NULL autorisé
	|ADD SCOPE <table> -- Table cible pour REFERENCES
	|SET DATA TYPE <type>} -- Type de données
}}}

=== Suppression d'une table ===

`DROP` supprime un objet, souvent une table comme pour `CREATE`:
{{{sql
DROP TABLE <table>;
}}}

== Data Control Language ==

Le DCL comprend l'attribution ou la révocation de privilèges par utilisateur ou
rôle. Chaque privilège correspond à une action (insertion, suppression...) sur
un objet (table, colonne...):
{{{sql
{SELECT [(<cols>)] -- Droit de sélection
|DELETE -- Droit de suppression
|INSERT [(<cols>)] -- Droit d'insertion
|UPDATE [(<cols>)] -- Droit de modification
|ALL PRIVILEGES -- Tous les droits
} ON <obj> -- Objet sur lequel donner les droits
}}}

=== Rôles ===

`CREATE` et `DROP` respectivement créé et supprime un rôle:
{{{sql
CREATE ROLE <role>; -- Créer un rôle
DROP ROLE <role>; -- Supprime un rôle
}}}

`GRANT` et `REVOKE` contrôlent les privilèges des rôles.

=== Attribution ===

`GRANT` attribue un privilège ou des rôles à des utilisateurs et rôles:
{{{sql
GRANT {<privilege>|<roles>} TO {<users>,<roles>};
}}}

=== Révocation ===

`REVOKE` révoque un privilège ou des rôles à des utilisateurs et rôles:
{{{sql
REVOKE {<privilege>|<roles>} FROM {<users>,<roles>};
}}}

== Liens externes ==

* [[https://en.wikipedia.org/wiki/SQL_syntax]]
* [[https://en.wikibooks.org/wiki/Structured_Query_Language]]
* [[https://jakewheat.github.io/sql-overview/]]
