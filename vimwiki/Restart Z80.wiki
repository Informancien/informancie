= Restart Z80 =

Références:
* « La bible du programmeur de l’Amstrad CPC », BruckMann, Lothar, English,
  Gerits, p7-9, p116, Ed. Micro Application;

Les RST sont numérotés de 0 à 7 et sautent en RAM, respectivement en : `00h`, `08h`,
`18h`, `20h`, `28h`, `30h` et `38h`. Chaque exécution d’un `rst` entraîne un `push` du PC et
retourne à l'appelant après.

L’opcode d’un `rst` est d’un octet, c’est une manière très rapide de faire `call`.
L’implémentation dépend de la machine.

[[Restart Amstrad CPC|Exemple d'implémentation sur un Amstrad CPC]]
