= Paramètres de révision Git =

Les sous-commandes de `git` agissants sur des commits prennent en argument des
paramètres de révision (voir *gitrevisions(7)*), qui en les combinants
permettent de sélectionner un ou plusieurs commits sur lesquels agir.

Quand un commit est donné par son nom (hash, tag ou ref ou nom relatif) en
argument d'une commande parcourant le graphe, elle agira sur tous les commits
*joignables* depuis celui-ci. Exemples:
* `git log master` affichera les commits joignables depuis `master`, donc tous
  les commits participants à la branche;
* `git log master dev` affichera les commits joignables depuis `master` ou
  `dev`.

== Adressage relatif ==

Un commit peut être adressé relativement à un autre, grâce au suffixes suivants:
* `A~N` Le `N`ième commit précédent `A`, le parent directe si `N` est absent;
* `A^N` Le `N`ième parent du commit de merge `A`, le premier parent si `N` est
  omit.

Les suffixes peuvent être enchaînés, chacun prend comme repère le commit désigné
par le suffixe précédent. Exemples:
* `master~`, `master^` ou `master~1` est le parent de `master`;
* `master~2` ou `master^^` est le grand-parent de `master`;
* `master^2` est le second parent de `master`;
* `master^2~2` est le grand-parent (`~2`) du second parent (`^2`) de `master`;
* `master~3^2^` est le premier parent (`^`) du second parent (`^2`) de
 l'arrière-grand-parent (`~3`) de `master`.

TODO Graphe d'exemple.

== Intervalles ==

Les intervalles permettent de limiter le parcour du graphe de révisions à un 
ensemble précis de commits consécutifs.

=== Exclusion ===

`^A` exclu les commits joignables depuis `A`, qui est lui même exclu:
* `^dev` sélectionne les commits non joignables depuis `dev`, donc ne
  participant pas à cette branche;
* `^master dev` sélectionne les commits participant la branche `dev`, mais non
  `master`.

== Intervalle `..` ==

Des intervalles de commits peuvent être sélectionnés par la notation `A..B`:
sont pris les commits joignables depuis `B` mais non depuis `A`. Exemples:
* `master~2..master` sélectionne les commits `master` et `master~1`;
* `dev..master` sélectionne les commits participants à la branche `master`, mais
  non `dev`.

Un intervalle `A..B` est parfaitement égale à la sélection par exclusion `^A B`.

=== Différence symétrique `...` ===

La notation `A...B` sélectionne la différence symétrique (équivalent au ou
exclusif) des commits joignables depuis `A` ou `B`. Les commits doivent être
joignables exclusivement depuis `A` ou `B`, ils seront exclu s'ils sont
joignables par `A` et `B` en même temps. Par exemple `master...dev` sélectionne
les commits participants exclusivement soit `master`, soit à `dev`.

Cet intervalle sélectionne donc tout les commits deux branches jusqu'à leur
point de divergence.
