= Enoncé logique =

En logique classique, un _énoncé_ est constitué d'_opérateurs_ ou
_connecteurs logiques_, liants des _prédicats_ portants sur des _objets_. Les
_quantificateurs_ indiquent la portée de l'énoncé dans l'ensemble considéré.
Les parenthèses sont utilisées pour grouper l'énoncé en sous-parties. 

== Objets et classes ==

Un énoncé porte sur des objets désignés par des _termes_. 

D'abord les _constantes_, comme $5$ ou $N$, désignent des objets spécifiques.
Lorsqu'elles sont représentées par des lettres, elles sont notées par
convention en majuscule.

Les _variables_ désignent des objets indeterminés, par exemple "le nombre réél
$x$". Elles sont courament notées par des lettres minuscules latines ou 
grecques.

Chaque objet appartient à une classe, par exemple Orwell est un objet de la
classe humain, et 5 un objet de la classe entier. Cette notion est primordiale,
car elle permet de réutiliser l'énoncé grâce aux variables: n'importe quels
objets de la bonne classe pourront leurs être assignés. Si un énoncé dicte que
"la variable $a$ est un entier", alors tout nombre entier pourra convenir à
cette variable.

Les objets mentionnés dans l'énoncé par les constantes, ou substituables aux 
variables, sont regroupés dans un _ensemble de base_.

Les termes peuvent être regroupés par des _symboles de fonction_ pour créer des
termes plus complexes. $x \cdot y + 1$ est un unique terme en combinant
d'autres par les symboles $\cdot$ et $+$.

Ces notions, entre autre le découplage de l'énoncé des objets et l'ensemble de
base, sont formalisées par les [[Structures logiques|structures]].

== Prédicat ==

Un prédicat est un fait qu'on évalue comme vrai ou faux, respectivement notés 1
et 0, tel une propriété d'un objet ou une relation entre plusieurs d'entre eux.
Ils sont nommés par des _symboles de prédicat_, leurs _arité_ donne le nombre
d'objets sur lesquels ceux-ci portent.

La forme la plus simple de prédicat est la _constante propostionnelle_,
décrivant un fait simple, comme $il-fait-beau$ ou $nous-somme-en-été$. Ces
prédicats sont indécomposables et ne portent sur aucun objet, ils sont donc
d'arité 0.

Les symboles _unaire_ portent sur un unique objet et en donne une propriété, par
exemple $n est-pair$. 

Les symboles d'une arité supérieure à un, comme ceux _binaire_ (arité 2) ou 
_ternaire_ (arité 3) décrivent une relation entre leurs objets, comme $x < y$
ou $z y et z sont-alignés$.

Les symbôles de prédicats sont souvent notés en préfixes avec parenthèses, par
exemple le symbole ternaire $P$ sera écrit $P(x, y, z)$. La notation en infixe
est souvant utilisées pour les symboles binaires courants comme $x = y$ ou
$a \leq b$.

== Connecteurs logiques ==

Les connecteurs logiques regroupent les prédicats en assertions plus grandes,
dont la véracité (vrai ou fausse, 1 ou 0) est évaluée selon leurs
_tables de vérité_.

=== Négation ===

L'opérateur unaire de négation $\neg$, lu "non", inverse la valeur d'un
prédicat. Si le prédicat $A$ est vrai, alors $\neg A$, "non A", est faux, et
inversement.

| A | $\neg A$ |
|---|----------|
| 1 | 0        |
| 0 | 1        |

La négation d'une relation peut être indiqué par la rature du symbole de
prédicat: $\neg x = y$ est équivalent à $x \ne y$.

=== Conjonction ===

La conjonction, de symbole $\wedge$ est vraie si ses deux prédicats le sont.
$A \wedge B$ se dit "A et B".

| A | B | $A \wedge B$ |
|---|---|--------------|
| 0 | 0 | 0            |
| 0 | 1 | 0            |
| 1 | 0 | 0            |
| 1 | 1 | 1            |

=== Disjonction ===

La disjonction $\vee$ est vraie si l'un de ces deux prédicats est vrai, et se
lit donc "ou": $A \vee B$, "A ou B" indique qu'une seule des conditions suffit.

| A | B | $A \vee B$ |
|---|---|------------|
| 0 | 0 | 0          |
| 0 | 1 | 1          |
| 1 | 0 | 1          |
| 1 | 1 | 1          |

Là ou la conjonction est vrai si les deux prédicats l'est, la
_conjonction exclusive_ $\oplus$ n'est vrai que si *un seul* des prédicat l'est.
Elle peut se lire "ou bien", impliquant que les deux propositions sont
imcompatibles.

| A | B | $A \oplus B$ |
|---|---|--------------|
| 0 | 0 | 0            |
| 0 | 1 | 1            |
| 1 | 0 | 1            |
| 1 | 1 | 0            |

=== Implication ===

L'implication $\Rightarrow$ donne une relation de cause à effet entre deux
prédicats, si le premier est vrai, l'_antécédant_, alors le second, le
_conscéquent_, l'est alors forcement. En français, cela pourrais donner "_s_'il
fait froid _alors_ je met un manteau", "faire froid" est l'antécédant qui
implique comme conscequent "mettre un manteau". $A \Rightarrow B$ peut se lire
"si A alors B", "A implique B" ou "A seulement si B".

| A | B | $A \Rightarrow B$ |
|---|---|-------------------|
| 0 | 0 | 1                 |
| 0 | 1 | 1                 |
| 1 | 0 | 0                 |
| 1 | 1 | 1                 |

On notera que $A \Righarrow B$ est équivalent à $\neg A \vee B$.

=== Equivalence ===

Deux prédicats sont notés comme équivalent avec $\Leftrightarrow$, cela veut
dire qu'ils sont soit tout deux vrai, ou tout deux faux.

| A | B | $A \Leftrightarrow B$ |
|---|---|-----------------------|
| 0 | 0 | 1                     |
| 0 | 1 | 0                     |
| 1 | 0 | 0                     |
| 1 | 1 | 1                     |

L'équivalence peut être recréé à partir de l'implication: $A \Leftrightarrow B$
donne le même résultat que $(A \Rightarrow B) \wedge (B \Rightarrow A)$.

== Quantificateurs ==

Les quantificateurs indiquent combiens d'objets de l'ensemble considérés,
assignés à une variables, doivent vérifier l'expression pour que celle si soit
vraie. Par exemple, on peut dire "_au moins une_ bille est rouge": le
quantificateur "au moins une" nous dit que du moment qu'une ou plus de billes
de l'ensemble concerné est rouge, alors ce que l'on dit est vrai. Ici, "bille"
est la variable à laquelle on assigne une à une chacune des billes pour
vérifier cette condition.

La variable sur laquelle le quantificateur porte est dite _liée_, celles sans 
quantificateurs sont _libres_. Par exemple dans "il n'y a qu'une seul planète
habitable", le quantificateur "il n'y a qu'une seule" lie la variable "planète"
avec la propriété "habitable".

=== Quantificateur universel ===

Le _quantificateur universel_ $\forall$ se lit "pour tout" ou "quel que soit".
Il indique que l'expression doit se vérifier pour tout les éléments de
l'ensemble considéré, assignables à la variable liée. $\forall x x > 0$ se lit
"pour tout x, x est supérieur à 0" est signifie "tout objet de l'ensemble
considéré est supérieur à zéro".

=== Quantificateur existentiel ===

Le _quantificateur existentiel_ $\exists$ "il existe (au moins) un ... tel que"
dit qu'au moins un objet de l'ensemble considéré vérifie l'expression:
$\exists x x = 0$ est lu "il existe un x égale à zéro".

L'_unicité_ est exprimé par le quantificateur $\exists!$ qui signifie "il existe
un *unique* ... tel que". Un seul et unique objet de l'ensemble considéré doit
vérifier l'expression.
