== Stabilité du CRTC en fréquence ==

Sources
* Frame flyback and interrupts: https://www.cpcwiki.eu/forum/programming/frame-flyback-and-interrupts/msg25106/#msg25106

=== Stabilité verticale ===

Programmé dans le CPC, le CRTC affiche un écran de 200 lignes visibles :
R6*(R9+1) = 25*8. Cependant, pour compter toutes les lignes, il faut tenir
compte des lignes invisibles : les bordures et les hauteurs des VBL.

Il y a deux bordures verticales : une avant la VBL (qui apparaît en bas), et une
après (qui apparaît en haut).

* Pour la première bordure : (R7-R1)*(R9+1) = (30-25)*8 = 40 scanlines;
* Pour la VBL : b4b7 de R3 = 8 scanlines;
* Pour la deuxième frontière : (R4-R7)*(R9+1) = (38-30)*8 = 64 scanlines.
 
Si nous additionnons ces chiffres, nous obtenons 312 scanlines. Une autre façon
de calculer est de prendre (R4+1)*(R9+1)+R5 = 39*8+0 = 312 scanlines.

Nous savons qu'une ligne de trame correspond à 64 NOP. Un NOP prenant 1
microseconde, une image à afficher prendra 64*312*1 = 19968 microsecondes. Ce
qui donne près de 50 images par seconde : c'est la fréquence du moniteur et il
faut l'observer. Avec un moniteur CPC, il est donc très important, pour garder
un écran stable, d'avoir toujours 312 lignes de trame (le moniteur accepte une
petite marge, plus étroite sur un CRTC2).

Dans le cas de plusieurs écrans par frame (technique de la rupture), c'est bien
sûr la somme de (R4+1)*(R9+1)+R5 de tous les écrans qui doit être égale à 312. 

=== Stabilité horizontale ===

Chaque ligne doit faire au total 64us et chaque hsync doit faire 6us.
