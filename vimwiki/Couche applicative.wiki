= Couche applicative =

La _couche applicative_, _logique_ ou _composant_ décrit le fonctionnement des
applications identifiées lors de l'analyse de la [[Couche fonctionnelle|couche fonctionnelle]].

== Points d'analyse ==

On décompose cette couche en trois types d'éléments:
* Les *flux*, leurs protocoles (port), correspondances aux [[Couche fonctionnelle#Analyse|flux métiers]],
  volume, fréquence, latence et *sens d'initialisation* (règles des parefeu);
* Les *gisements de données*, leurs nature, filtrage, format (base de donnée,
  système de fichier...), droits d'accès, et durée de vie;
* Les *intergiciels* (_middleware_ en anglais) qui sont les applications qui
  instancient et gèrent les deux élément précédents, tel les serveurs, bases de
  données et services de sécurité.
