= Groupes de contrôle =

Les _groupes de contrôles_ ou _cgroups_ permettent de grouper des processus et
de contrôler et surveiller leurs accès aux ressources comme la mémoire, le
processeur ou les périphérique.

Les cgroups sont divisés en _sous-systèmes_, _modules_ ou _contrôleur_, ayant
chacun une fonction de contrôle d'une ressource.

TODO Approfondir la structure des cgroups don cgroupfs

