= Conteneur privilégié et non-privilégié =

== Conteneur privilégié ==

Dans un conteneur privilégié, l'utilisateur root de l'invité correspond à
l'utilisateur root de l'hôte, toutes les tâches d'administrations sont donc
autorisées dans le conteneur. Ceci est extrêmement risqué en environnement de
sensible: l'utilisateur root du conteneur, peut, par des appels systèmes
standards, remonter à l'hôte, sur lequel il sera aussi administrateur.

== Conteneur non-privilégié ==

Un conteneur non-privilégié translate les utilisateurs dans le conteneur à un
autre sur l'hôté via les espaces de noms utilisateurs. L'utilisateur root dans
le conteneur sera translaté vers un autre utilisateur lors des appels systèmes.
Cela empêche que l'utilisateur root d'un conteneur puisse prendre le contrôle de
l'hôte, mais bloquera les appel systèmes comme la création ou le montage de
blocs.

=== Configuration ===

Les configurations suivantes sont lues lors de l'utilisation d'un conteneur 
non-privilégié:
* `~/.config/lxc/lxc.conf` Configuration système;
* `~/.config/lxc/default.conf` Configuration par défaut des conteneurs;
* `~/.local/share/lxc/` Dossier des conteneurs;
* `~/.cache/lxc/` Cache.

Le mappage est précisé dans `~/.config/lxc/default.conf`, par l'option
`lxc.id_map`:
{{{
lxc.id_map = {u|g} <first_id> <range_start> <range_count>
}}}

Les champs de la valeur sont:
* `{u|g}` Le type de mappage, `u` pour les UIDs, `g` pour les GIDs. L'option
  doit donc être répétée deux fois pour préciser les deux mappages;
* `<first_id>` Le premier UID ou GID tel que vu dans le conteneur, souvent `0`;
* `<range_start>` et `<range_count>` Respectivement le début puis la taille de
  la plage d'UIDs ou de GIDs auxquels ceux-ci pourront correspondre sur l'hôte.
  Voir les fichiers `/etc/subuid` et `/etc/subgid`, *subuid(5)* et *subgid(5)*.

Exemple d'un mappage:
{{{
lxc.id_map = u 0 10000 1000
lxc.id_map = g 0 10000 1000
}}}

TODO Exemple avec subuid/subgid

=== lxc-user-nic ===

La commande `lxc-user-nic` (voir *lxc-user-nic(1)*), avec le droit SETUID
positionné, permet à l'utilisateur propriétaire d'un conteneur non-privilégié de
configurer le [[Réseau LXC#Bridgé|bridge]] sur l'hôte. Le fichier de
configuration `/etc/lxc/lxc-usernet` permet de limiter les capacités de
l'utilisateur, chaque ligne est au format:
{{{
{@<group>|<user>} veth <bridge> <count>
}}}

Les champs de la lignes sont:
* `{@<group>|<user>}` Le nom du groupe (préfixé par `@`) ou de l'utilisateur
  concerné;
* `veth` Le type d'interface que peut créer l'utilisateur, seul `veth` est
  supporté par LXC 2.0;
* `<bridge>` Le nom du bridge sur lequel les interfaces peuvent être créés;
* `<count>` Le nombre maximal d'intefaces que peut créer l'utilisateur sur le
  bridge.

== Liens externes ==

* [[https://stgraber.org/2014/01/17/lxc-1-0-unprivileged-containers/]]
