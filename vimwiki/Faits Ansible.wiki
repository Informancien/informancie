= Faits Ansible =

Ansible, à chaque application d'un play sur un managed node, éxecute
systématiquement le module `setup`, réalisant sur ce node une collecte de
_faits_, c'est-à-dire diverses informations sur l'hôte et le système. La
variable de type tableau `ansible_facts` est peuplé avec les faits collectés sur
l'hôte courant. `<fact>` est le fait voulu:
{{{yaml
{{ ansible_facts['<fact>'] }}
}}}

A noter que le tableau `ansible_facts` est multidimensionnel, et contient donc
d'autres sous-tableaux, regroupant les faits par sujet.

Les faits peuvent être lus pour n'importe quel managed node, dans tout contexte,
grâce à la variable `hostvars`. `<host>` est le nom de l'hôte dont on souhaite
connaître le fait `<fact>`:
{{{yaml
{{ hostvars['<host>']['ansible_facts']['<fact>'] }}
}}}

La commande ad-hoc `ansible <host> -m setup` liste les faits disponibles sur
l'hôte `<host>`.

La collecte de faits peut être inhibée pour un play avec la clé `gather_facts`
valuée à `no`.

== Faits personnalisés ==

Des faits personnalisés peuvent être collectés en ajoutant des fichiers INI,
JSON, ou exécutables retournant du JSON, tous avec l'extension `.fact`, au
dossier `/etc/ansible/facts.d` des managed node.

e dossier des faits personnalisés peut être changé avec l'argument `fact_path`
du module `setup`.
