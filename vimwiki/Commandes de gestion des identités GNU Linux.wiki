= Commandes de gestion des identités GNU/Linux =

== Utilisateur ==

`useradd <login>` Créer l'utilisateur `<login>`:
* `-m` Créer le répertoire utilisateur dans `/home`;
* `-g <group>` Groupe principal;
* `-G <group>[,...]` Groupes secondaires;
* `-u <uid>` UID;
* `-d <path>` Chemin du dossier utilisateur;
* `-c <comment>` Commentaire stocké dans `/etc/passwd`;
* `-s <path>` Shell de connexion;
* `-p <hash>` Mot de passe hashé,
  *déconseillé car le hash reste dans l'historique des commandes*.

`usermod <login>` Modifier l'utilisateur `<login>`:
* `-g <group>` Groupe principal;
* `-[a]G <group>[,...]` Groupes secondaires, `-a` les ajoutes aux courants;
* `-[m]d <path>` Chemin du dossier utilisateur, `-m` déplace le dossier courant;
* `-l <login>` Nouvel identifiant;
* `-u <uid>` UID;
* `-c <comment>` Commentaire stocké dans `/etc/passwd`;
* `-s <path>` Shell de connexion;
* `-p <hash>` Mot de passe hashé,
  *déconseillé car le hash reste dans l'historique des commandes*.

`passwd <login>` Modifier le mot de passe de l'utilisateur `<login>`:
* `-d` Supprimer le mot de passe, rend la connexion impossible;
* `-[a]S` Afficher le status du compte, de tous le comptes avec `-a`.

`userdel <login>` Supprimer l'utilisateur `<login>`:
* `-r` Supprime le dossier utilisateur.

`vipw` Ouvrir `/etc/passwd` avec l'éditeur:
* `-s` Ouvrir `/etc/shadow`.

`pwck` Vérification de la cohérence de `/etc/passwd`.

== Groupe ==

`groupadd <group>` Créer le groupe `<group>`:
* `-g <gid>` GID.

`groupmod <group>` Modifier le group `<group>`:
* `-g <gid>` GID;
* `-n <name>` Changer le nom.

`groupdel <group>` Supprimer le groupe `<group>`.

`vigr` ou `vipw -g` Ouvrir `/etc/group` dans l'éditeur:
* `-s` Ouvrir `/etc/gshadow`.

`grpck` Vérification de la cohérence de `/etc/group`.
