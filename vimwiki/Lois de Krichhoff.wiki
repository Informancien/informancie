= Lois de Kirchhoff =

== Loi des nœuds ==

La somme des intensités entrantes dans un [[Lexique#Nœud|nœud]] est égale à la
somme des intensités sortantes.

== Loi des mailles ==

La somme des differences de potentiels dans une [[Lexique#Maille|maille]] est
toujours nulle.
