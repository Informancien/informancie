= HPLIP =

HPLIP (HP Linux Imaging and Printing) est une solution d'impression et de
numérisation libre (licences GNU GPL, MIT et BSD) développée et activement
maintenue (à cette date, 2021) par HP, supportant ses imprimantes et scanners 
sous GNU/Linux.

== Liens externes ==

* [[https://developers.hp.com/hp-linux-imaging-and-printing/supported_devices/index]]
