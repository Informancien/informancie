= Fractions =

Une fraction est la symbolisation d'une division, dont le résultat, le
_quotient_ est un nombre rationnel, c'est-à-dire qui peut être représenté par
la fraction de deux nombres entiers. On ecrit la fraction, de $x$ par $y$:
{{$
\frac{x}{y}
}}$

Le membre du dessus est le _numérateur_, celui du dessous le _dénominateur_. Le
dénominateur *doit être différent de 0*, le quotient pour cette valeur n'est
pas définie: on ne peut diviser par 0.

On appel le résulat le _rapport_, qui est en quelque sorte la réciproque
du produit de la multiplication. Le rapport est définit comme le nombre par
lequel le dénominateur doit être multiplié pour obtenir le numérateur. Ceci est
démontré par l'équation suivante:
{{$
\begin{align}
\frac{x}{y} &= x\\
x &= x \cdot y
\end{align}
}}$

On peut donc comprendre le terme de rapport comme la "relation" entre les deux
membres de la fraction. Plus simplement, le nombre de fois que peut passer le
dénominateur dans le numérateur.

== Propriétés ==

=== Addition de fractions ===

Pour additionner deux fractions, il faut les mettres au même dénominateur, on
additionne ou soustrait ensuite leurs numérateurs. La méthode la plus simple 
pour égaliser les dénominateurs consiste à multiplier les deux membres de
chaque fraction par le dénominateur de l'autre, on peut ensuite réaliser
l'opération:
{{$
\frac{a}{b} + \frac{c}{d} =
\frac{a \cdot d}{b \cdot d} + \frac{c \cdot b}{d \cdot b} =
\frac{a \cdot d + c \cdot b}{b \cdot d}
}}$

=== Multiplication de fractions ===

Multiplier deux fractions revient à multiplier leurs numérateurs et
dénominateurs entre eux:
{{$
\frac{a}{b} \cdot \frac{c}{d} =
\frac{a \ cdot c}{b \cdot d}
}}$

=== Division d'une fraction ===

Diviser une fraction par une autre équivaut à la multiplier par son
[[Inverse d'un nombre#Inverse d'une fraction|inverse]]:
{{$
\frac{a}{b} / \frac{c}{d} =
\frac{a}{b} \cdot \frac{d}{c}
}}$
