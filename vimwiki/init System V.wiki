= init System V =

Pour finir sa séquence d'amorçage, le noyau Linux instancie le premier processus
de l'espace utilisateur, `init` (voir *init(8)*), identifié par le PID 1 et qui
est conscéquement parent de *tous les autres processus*. C'est là même son rôle,
lancer les processus configurants le système et gérant les terminaux.

Le traditionnel programme GNU/Linux `init` hérite son fonctionnement de celui
d'AT&T Unix System V (chiffre romain cinq). Les processus à instancier sont
regroupés en _niveaux d'exécution_, aussi dits _runlevels_ ou _init levels_,
représentés par les chiffres de `0` à `6`, et la lettre `S`, sans considération
pour sa casse. Pour chaque niveau est définit un ensemble de commandes à
exécuter, et comment `init` doit contrôler leur exécution.

Quatres des niveaux d'exécution sont réservés, et décrivent un état particulier
du système:
* `S` (ou `s`) Mode mono-utilisateur (_single mode_ en anglais) utilisé au
  démarrage du système,
* `0` Arrêt du système,
* `1` Mode mono-utilisateur atteignable depuis un mode multi-utilisateur, passe
  immédiatement en mode `S`,
* `6` Redémarrage du système.

Conventionellement, les niveaux de `2` à `5` établissent les environnements
suivants:
* `2` Mode multi-utilisateurs, sans réseau,
* `3` Mode multi-utilisateurs, avec réseau,
* `4` Indéfinie, configurable par l'administrateur,
* `5` Mode multi-utilisateur avec interface graphique via X.

== Séquence de démarrage ==

A son démarrage, `init` se positionne en niveau `S`, et tente ensuite de passer
au niveau précisé par l'entrée `initdefault` du fichier `/etc/inittab`, décrit
dans la prochaine section de cette page.

Dans le cas ou l'entrée `initdefault` manque au fichier, `init` invitera
interactivement l'utilisateur à choisir le niveau d'exécution.

Si le fichier `/etc/inittab` est manquant, ou qu'`init` rencontre un problème
nécessitant l'intervention de l'administrateur (par exemple l'impossibilité de
monter un système de fichiers), alors il reste en niveau `S`. A ce niveau
d'exécution, `init` ne requiert aucune configuration et lance le programme
`/sbin/sulogin` attaché à la console `/dev/console`: l'administrateur sera
invité à saisir le mot de passe du compte `root` pour instancier son shell.

Attention, si le compte `root` est bloqué, *il sera impossible de se connecter*
en tant que `root` en niveau `S`. Ceci est une sécurité pour empêcher
l'utilisateur d'accèder à un shell privilégié en forçant l'amorçage en mode `S`.

Au moment ou `init` entre pour la première fois en mode multi-utilisateurs,
après le démarrage en niveau `S`, les entrées `boot` et `bootwait` du fichier
`/etc/inittab` sont lancées, quel que soit le niveau multi-utilisateurs ciblé.
L'utilisation principale de ces entrées est le montage des systèmes de fichiers.

Enfin, les entrées d'`/etc/inittab` décrivant le niveau cible sont exécutées.

TODO Diagramme de flux séquance de démarrage

== /etc/inittab ==

Le fichier `/etc/inittab` définie les actions qu'`init` doit prendre pour
atteindre les niveaux d'exécution de `0` à `6`, ainsi que pour ceux
_à la demande_ `A`, `B` et `C` (sans sensibilité à la casse). Aucune action ne
peut être définie pour le niveau `S`, toujours réservé au mode mono-utilisateur,
car celui-ci ne requiert pas l'existence d'`/etc/inittab`.

Les lignes ouvertent par le caractère `#` sont des commentaires. Chaque ligne du
fichier prend sinon cette syntaxe:
{{{
<id>:<runlevels>:<action>:<cmd>
}}}

Les champs définissent pour cette entrée:
* `<id>` L'identifiant unique de l'entrée, de 4 caractères maximum (2 caractères
  pour certaines anciennes implémentations), ne détermine aucunement le
  comportement d'`init`,
* `<runlevels>` Liste des niveaux d'exécution concernés par l'entrée. Les
  caractères de niveaux sont attachés, par exemple `234` indique les niveaux
  `2`, `3` et `4`,
* `<action>` L'action que doit prendre `init` lors de l'entrée dans l'un des
  niveaux donnés par le champ `<runlevels>`,
* `<cmd>` Commande à exécuter lors de l'entrée dans l'un des niveaux du champ 
  `<runlevels>`.

Le champ `<action>` determine à quelle étape `init` lancera la commande `<cmd>`,
et comment son exécution sera controllée (voir *inittab(5)* pour la liste
exhaustive):
* `initdefault` Indique à `init` que le niveau d'exécution par défaut est celui
  donné par le champ `<runlevels>`. Le champ `<cmd>` est ignoré avec cette
  action,
* `sysinit` La commande sera exécutée au plus tôt dans le démarrage du système,
  sans considération pour le champ `<runlevels>`,
* `boot` La commande sera exécuté au démarrage, après les entrées `sysinit`. Le
  champ `<runlevels>` est aussi ignoré,
* `bootwait` Idem à `boot`, mais `init` attendra la fin de la commande avant de
  continuer. `<runlevels>` est toujours ignoré,
* `once` Lors du passage dans un niveau de `<runlevels>`, `<cmd>` sera exécutée,
* `wait` Lors du passage dans un niveau de `<runlevels>`, `<cmd>` sera exécutée
  et `init` attendra sa fin pour continuer,
* `respawn` Idem à `once`, mais `init` relancera la commande si jamais celle-ci
  se termine. Cette action est souvent utilisée pour les gestionnaires de
  terminaux tels `getty`, et c'est donc `init` qui se charge de les relancer
  lorsqu'ils sont arrêtés, par exemple par la commande `exit` lors d'une session
  de shell interactif,
* `off` L'entrée est ignorée,
* `ondemand` Pour les niveaux d'exécutions à la demande `A`, `B` et `C`. La
  commande est exécutée en restant dans le niveau courant, voir le changement à
  chaud dans la section suivante,
* `ctrlaltdel` La commande est exécutée lorsqu'`init` reçoit le signal `SIGINT`,
  envoyé lorsque l'utilisateur enfonce simultanément `Ctrl`, `Alt` et `Suppr` au
  clavier sur la console système (c'est-à-dire `/dev/console` ou `/dev/tty1`).

== Changement de niveau à chaud ==

`init` est capable de changer de niveau d'exécution à chaud, après le démarrage
du système. Ceci est fait par l'invocation de la commande `init <n>` ou
`telinit <n>`, l'argument `<n>` est le niveau d'exécution ciblé (de `0` à `6`,
`S` pour le mode mono-utilisateur et `A`, `B` ou `C` pour les niveaux à la
demande), sinon l'une des directives suivantes, insensibles à la casse:
* `Q` Recharger le fichier `/etc/inittab` et réaliser l'arrêt et le démarrage
  des processus en conscéquence,
* `U` Relancer le niveau d'exécution courant, sans relire `/etc/inittab`.

Lors du changement de niveau d'exécution, `init` enverra au processus qui ne
font pas (ou plus) partis du niveau cible le signal `SIGTERM` afin qu'ils
s'arrêtent. Dans le cas ou ils ne cèssent pas au bout de cinq secondes, `init`
les tueras avec le signal `SIGKILL`. L'option `-t <secs>` de `telinit` ou `init`
passe le délais avant déstruction des processus à `<secs>` secondes au lieu de
cinq.

Bien noter que lors d'un changement de niveau à chaud, les actions `sysinit`,
`boot` et `bootwait` ne *sont pas exécutées à nouveaux*: celles-ci sont
resérvées au démarrage du système.

TODO === Alimentation critique ===

== Services ==

Un _service_ ou _script d'initialisation_ conforme Système V est un simple
script shell, le plus couramment contenus dans le dossier `/etc/init.d/` (ou
`/etc/rc.d/` en fonction des distribution GNU/Linux), servant à configurer le
système ou à gérer un démon, processus fonctionnant continuellement en
arrière-plan (tel un serveur [[SSH]]), et devant être instancié lors de l'entrée
dans un certain niveau d'exécution (lors du démarrage ou d'un changement à
chaud).

Ces scripts de services prennent en premier argument une action à lui faire
réaliser:
* `start` Démarrer le service,
* `stop` Arrêter le service,
* `status` Afficher l'état du service,
* `reload` Recharger la configuration du service,
* `restart` Redémarrer le service,
* `try-restart` Arrêter le service puis essayer de le relancer si l'arrêt c'est
  produit normalement.

Un script de service doit *obligatoirement* comprendre les arguments `start` et
`stop`, les autres sont optionnellement pris en charge.

La plupart des distributions GNU/Linux fournissent la commande
`service <script> <arg> [<opt> ...]`, invoquant le script d'initialisation
`<script>` avec l'argument `<arg>` et les options `<opt> ...`. Par exemple,
démarrer le service `cups` via la commande `service cups start` est équivalent
à l'appel direct `/etc/init.d/cups start`.

=== Script rc ===

Les services peuvent être très nombreux sur un système, et ne sont donc pas tous
instanciés et gérés individuellement par `init`: le script d'initialisation
`/etc/init.d/rc` (son nom et chemin peuvent varier d'une distribution à une
autre) s'en charge. On observe alors ces lignes dans `/etc/inittab`, ou `<n>`
est le niveau d'initialisation concerné:
{{{
:<n>:wait:/etc/init.d/rc <n>
}}}

Le script `rc` prend en argument un niveau d'exécution (si aucun n'est donné,
celui de l'entrée `initdefault` de `/etc/inittab`), et se chargera de mettre en
place exclusivement les services qui lui sont associés. Pour définir lesquels
doivent être démarrés ou arrêtés lors du passage au niveau `<n>`, un lien
symbolique vers le service concerné (pour rappel, script dans `/etc/init.d`,
selon la distribution) est créé dans le dossier `/etc/rc<n>.d` (ce dossier
peut aussi varier d'une distribution à l'autre).

Le nom d'un lien symbolique dans un dossiers `/etc/rc<n>.d` doit suivre la
syntaxe suivante, déterminante dans le comportement de `rc`:
{{{
{K|S}<priority><service>
}}}

Pour chacun des liens, lors du démarrage ou d'un changement de niveau, `rc`:
* Arrêtera le service en appelant le script pointé par le lien avec l'argument
  `stop`, si le nom de se dernier commence par `K` (Kill),
* Démarrera le service en appelant le script pointé par le lien avec l'argument
  `start`, si le nom de se dernier commence par `S` (Start).

Par convention et lisibilité, `<service>` correspond au nom de base du script de
service pointé par le lien.

`<priority>` détermine par deux chiffres l'ordre dans lequel les démarrages et
arrêts de services auront lieux: plus le nombre est petit, plus la priorité est 
élevée. Par exemple, lors du passage au niveau d'exécution `5`,
`/etc/rc5.d/S10network` sera démarré avant `/etc/rc5.d/S20sshd`.

TODO == Commande runlevel ==



== Liens externes ==

* [[http://fr.linuxfromscratch.org/view/lfs-7.2-fr/chapter07/usage.html]]
* [[https://unix.stackexchange.com/questions/3537/etc-rc-d-vs-etc-init-d]]
