= Langage machine et d'assemblage =

Le _langage machine_ est un ensemble d'_instructions_ comprises par une
architecture donnée. Une instruction est suite de bits intérpétée par le
microprocesseur comme une opération à exécuter sur certaines données.

Ces instructions sont stockées en mémoire centrale, et peuvent encodées sur un
ou plusieurs mots mémoire. Cet *encodage est spécifique à une architecture*,
une même suite de mots peut donner deux instructions différentes d'une
architecture à l'autre.

== Instruction ==

Une instruction se découpe et deux champs, le _champ opération_ et le
_champ opérande_. L'opération indique au processeur ce qu'il doit faire sur les
opérandes, les données sur lesquels l'instruction porte.

Le nombre de bits que contient le champ opération donne le nombre de
_codes opérations_ (souvent abrégé _codeop_ ou _opcode_) que peut intérpréter
le processeur: pour un champ de $n$ bits, il y a $2^n$ opérations possibles.
L'ensemble des codes opérations compris par une architecture est dit son 
_jeu d'instructions_.

TODO === Groupes ===

=== Opérande ===

Une opérande indique l'une des données sur laquelle porte une opération, la 
majorité des instructions contiennent une ou deux opérandes. La méthode
d'obtention de la donnée finale à partir de l'opérande est qualifiée par son
_mode d'adressage_:
* Le _mode d'adressage immédiat_, l'opérande est elle même la donnée sur
  laquelle l'instruction porte, par exemple le nombre décimal 10;
* Le _mode d'adressage registre_, l'opérande désigne un registre du processeur,
  contenant la donnée, par exemple le registre `R1`;
* Un _mode d'adressage direct_, la donnée est un mot en mémoire centrale,
  identifié dans l'opérande par son adresse, par exemple le mot à l'adresse
  `0xF150`;
* Le _mode d'adressage indirect_, le mot mémoire pointé par l'opérande
  contient *l'adresse de la donnée*, qu'il faut ensuite récupérer à nouveau
  en mémoire centrale;
* Le _mode d'adressage registre indirect_, l'opérande désigne un registre
  contenant l'adresse de la donnée en mémoire centrale. 

Le mode d'adressage indirect est plus lent que le direct: il est nécessaire de
lire une première fois l'opérande pour obtenir une adresse, puis effectuer une
seconde lecture en mémoire centrale pour avoir la donnée voulue.

L'adressage registre indirect sera aussi plus rapide que l'adressage indirect
(sur un mot en mémoire centrale), car l'adresse est déjà chargé dans le
processeur, une seule opération de lecture mémoire est alors requise. Il faudra
sinon charger l'opérande dans un registre depuis la mémoire, puis faire la
lecture finale.

Il faut aussi comprendre qu'une instruction peut aussi manipuler des registres
ou mots mémoire qui ne lui sont pas passés en opérandes. Par exemple, les
instructions arithmétiques peuvent utiliser toujours un même registre
accumulateur, ou encore les instructions de manipulation de pile, qui écriront
et liront en mémoire à une adresse qui n'est pas donnée par une opérande mais
un registre.

A noter aussi que d'autres modes d'adressages peuvent être offerts par chaque
architecture, n'ont étés décrit ici que les modes les plus courants.

=== Type de langage ===

Le _type de langage_ machine détermine les modes d'adressage des paires
d'opérandes que peut avoir ses instructions:
* Le _registre/mémoire_, l'une des opérande est un registre, l'autre un mot
  mémoire, type le plus souvent retrouvé;
* Le _mémoire/mémoire_, les deux opérandes sont des mots mémoire, cela permet
  de manipuler des données sans la contrainte du nombre de registres
  disponibles;
* Le _registre/registre_, les deux opérandes sont des registres.

La majorité des architectures offrent des instructions de plusieurs types,
c'est par exemple le cas du Zilog [[Z80]], qui propose des instructions
registre/mémoire et regsitre/registre.

Les types mémoire/mémoire et registre/mémoire sont privilégiés par les systèmes
CISC, tandis que le registre/registre est le type inhérent au RISC.

On comprend assez vite que les d'instructions registre/registre seront plus
rapide que les registre/mémoire, l'étant elles mêmes plus que les
mémoire/mémoire: pour les première aucune lecture en mémoire centrale n'est
nécessaire, pour les seconde une lecture l'est, et pour les troisièmes au
minimum deux. De plus, le mode d'adressage indirect ajoutera à chaque opérande
une opération de lecture supplémentaire.

TODO === Encodage ===

== Langage d'assemblage ==

Il est extrêment difficile de programmer efficacement en langage machine: cela
implique de connaître parfaitement l'encodage précis des instructions, des
codes opérations, opérandes et modes d'adressages. De plus, les claviers sont
conçues pour rediger du texte, et non des bits, compliquant encore la tâche.

Le _langage d'assemblage_ épargne le programmeur de cet écueil, dans lequel il
rédige son programme, qui est ensuite puis traduits en langage machine par un
_assembleur_ (par abus de langage, on appel très souvent le langage lui même
l'assembleur). Cette transformation du programme en langage machine est dite
l'_assemblage_.

En langage d'assemblage, un _mnémonique_ est associé à chaque code opération,
c'est un court mot plus facile à retenir et taper que la suite de bits du
langage machine. On peut par exemple avoir le mnémonique `nop` à la place de la
forme binaire de cette instruction, `00000000`.

Les opérandes, comme les chaînes de caractères et nombres, sont automatiquement
encodées par l'assembleur, par exemple l'opérande `'Hello world'` dans le code
d'assemblage sera traduite en son équivalent binaire ASCII (ou autre encodage
des caractères selon l'architecture). Une syntaxe particulière permet aussi de
définir le mode d'adressage des opérandes, par exemple `#F145` pour l'adressage
direct du mot à cette adresse, et `(#F145)` pour son adressage indirect.

Pour chaque élément du langage machine, son langage d'assemblage a un
équivalent. Ormis la syntaxe, il y a donc une correspondance sémantique totale
entre les deux langages.

=== Directives d'assemblage ===

Les assembleurs fournissent en plus diverses _directives d'assemblage_ pour
faciliter le développement. Elles sont intérprétées lors de l'assemblage, et ne
sont pas directement traduites en langage machine: les directives ne sont pas à
destination de la machine, mais de l'assembleur. Quelques types de directives
trouvées dans la majoritée des langages d'assemblage sont:
* Les _étiquettes_ (_labels_ en anglais), elles marquent des adresses mémoire
  dans le programme, plus faciles d'utilisation que les adresses absolues. Les
  adresses rééls des étiquettes sont calculées à l'assemblage, et substituées à
  leur références;
* Des _constantes_ réutilisables dans le programme, qui comme les étiquettes,
  sont substituées par l'assembleur.

== Langages de bas et haut niveaux ==

Une instruction donnée dans un langage machine n'est valable que dans son
architecture, un processeur peut intérpreter la suite de bits d'une instruction
différement qu'un autre. Les programmes en langage d'assemblage dépendent aussi
d'une architecure, un langage d'assemblage étant totalement homogéne à son
langage machine.

C'est pourquoi un programme en langage machine ou d'assemblage n'est pas
portable entre architectures, il ne fonctionnera que sur celle pour laquelle il
a été conçu. Pour pallier à ce problème, les _langages de hauts niveau_ ont
étés invités, tel le C. On les appels comme ceci par contraste avec les
langages machine et d'assemblage, dits _langages de bas niveau_: les premiers
reposent sur les langages machines, en lesquels ils sont transformés par
_compilation_ pour pouvoir être exécutés sur une machine cible.
