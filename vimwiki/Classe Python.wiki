= Classe Python =

Une classe Python est déclarée avec le mot clé `class`:
{{{python
class Earthling:
	"""Un terrien avec des pensées profondes"""

	counter = 0 # Nombre de terriens
	
	def __init__(self, name, thought):
		"""Fait penser thought à l'humain nommé name"""
		self.name = str(name)
		self.__thought = str(thought) 
		Human.counter += 1 
		
	def speakOut(self)
		"""Dire ses pensées tout haut"""
		print(sel.name + ' dit "' + self.__thought + '".')

	@classmethod
	def getCount(cls):
		"""Retourne le nombre de terriens"""
		return cls.counter
		
	@staticmethod
	def getBirthplace()
		"""Retourne la planète d'origine"""
		return "Terre"
}}}

La classe est instancié en appelant son nom comme une fonction:
{{{python
philip = Earthling("Philip K. Dick", \
	"Les androîdes rêvent-ils de moutons électriques?")
philip.speakOut()
}}}

A noter qu'une classe, comme tout en Python, est elle même un objet, de type
`classobj`. Si l'on déclare la classe `Earthling` précédente dans une console:
{{{
> >>> type(Earthling)
<type 'classobj'>
}}}

== Méthode d'instance ==

Une _méthode d'instance_ est déclarée dans le bloc `class` par le mot clé `def`,
dont le premier argument doit être `self`, qui est l'objet instancié. `self` est
*implicitement passé* lors de l'invocation, et ne doit pas être explicitement
donné. Par exemple, la méthode déclarée `def speakOut(self)` est invoquée par
`philip.speakOut()`.

== Constructeur et attributs d'instance ==

La méthode spéciale `__init__` est le constructeur, invoquée lors de 
l'instanciation avec les arguments donnés.
`Earthling("William Gibson", "Case est un sale type")` passera les deux argument
à `__init__`.

Un _attribut d'instance_ est déclaré dans le constructeur avec la notation en
`.`, par exemple `self.name = str(name)`. Il est accéssible sur l'objet par la
même écriture: `philip.name`.

== Encapsulation ==

Deux préfixes de nom permettent de définir le niveau d'accessibilité d'un membre
d'une classe:
* `_` Protégé, l'accès est limité la classe et ses enfants;
* `__` Privé, l'accès est exclusif à la classe.

Par exemple `self._prot_attr` et `self._protMethod()` sont hérités par les
enfants de la classe, `self.__pvt_attr` et `self.__pvtMethod()` lui sont
exclusifs

== Membre de classe ==

Un _membre de classe_ est associé à une classe et ces instances:
{{{python
# Depuis une instance
george = Earthling("George Orwell", "Big Brother is watching you.")
george.getCount() # Invocation
george.counter # Attribut

# Depuis l'objet de classe
Earthling.getCount() # Invocation
Earthling.counter # Attribut
}}}

Un _attribut de classe_ est déclaré directement dans le bloc de classe, tel
`counter` ci-dessous:
{{{python
class Earthling:

	counter = 0 # Nombre de terriens
}}}

Le décorateur `classmethod` fait de la méthode passée une _méthode de classe_.
Le premier argument de la méthode décorée doit être `cls`, l'objet de classe,
qui comme `self` est passé implicitement à l'invocation. Exemple:
{{{python
@classmethod
def getCount(cls):
	return cls.counter
}}}

Ou alors:
{{{python
def getCount(cls):
	return cls.counter
	
getCount = classmethod(getCount)
}}}

=== Précédence entre instance et classe ===

Si un membre d'instance porte le même nom qu'un de classe, il prendra la
précédence s'il est appelé depuis une instance. Exemple:
{{{python
class Precedence:
	
	same_name = "Attribut de classe"
	
	def __init__(self):
		self.same_name = "Attribut d'instance"
}}}

On peux vérifier la précédence sur la console:
{{{
> >>> instance = Precedence()
> >>> instance.same_name
'Attribut d'instance'
> >>> Precedence.same_name
'Attribut de classe'
}}}

== Méthodes statiques ==

Une méthode décorée avec `staticmethod` sera _statique_, elle appartient à la
classe mais n'a pas de premier argument implicite `self` ou `cls`: elle n'a donc
pas accès aux membres de classe ou l'instance:
{{{python
@staticmethod
def getBirthplace()
	"""Retourne la planette d'origine"""
	return "Terre"
}}}

Une méthode statique sont souvent utilisée pour associer une fonction utilitaire
en relation avec sa classe.
