= Journalisation noyau Linux =

Le noyau Linux utilise un tampon circulaire pour maintenir un journal de
messages. Ceux-ci sont envoyés via la fonction C `printk` depuis l'espace noyau,
ou dans de plus rares cas depuis l'espace utilisateur par écriture dans le
fichier virtuel `/dev/kmsg`, souvent lors du démarrage pendant lequel aucune
autre solution de journalisation n'est disponible.

TODO CONFIG_LOG_BUF_SHIFT (puissance de 2)
TODO dmesg_restrict

L'espace noyau enregistre entre autres nombre de messages traçants le
déroulement du démarrage (initialisation du matériel...), les évenements des
pilotes et [[Module noyau Linux|modules]] (par exemple le branchement à chaud
d'un périphérique ou le chargement d'un module), et les erreurs rencontrées. Le
contenu de se journal est donc un atout essentiel dans le débogage de l'espace
noyau.

Il faut bien comprendre que se journal ne concerne presque que l'espace noyau,
et ne devrait contenir que très peu, voir aucun message en provenance de
l'espace utilisateur. Les processus dans ce dernier utiliserons quant à eux un
service de journalisation (courament instancié par le processus d'initialisation
tel `init`) pour consigner des messages.

== Fichiers virtuels ==

Le fichier `/dev/kmsg` de type caractère abstrait le tampon du noyau, il permet
aux processus de l'espace utilisateur d'interagir avec celui-ci. Ceci est utile
lorsqu'aucun service de journalisation n'est disponible en espace utilisateur,
comme lors du démarrage, dans le contexte de l'initrd.

Le noyau expose son tampon au service de journalisation, tel [[syslogd]], via le
fichier `/proc/kmsg`. L'opération de lecture sur ce fichier est bloquante,
celle-ci n'aboutira que si le tampon n'est pas vide: son contenu est retourné
par l'opération puis vidangé, il est alors à la charge du service de
journalisation de consigner les messages lus.

== Niveaux de journalisation ==

Chaque message est associé à un _niveau de journalisation_ ou _log level_,
indiquant la nature du message:
0) `KERN_ERMEG` Urgence, le système est certainement innopérant;
1) `KERN_ALERT` Alerte, intervention immédiate requise;
2) `KERN_CRIT` Conditions critiques;
3) `KERN_ERR` Occurence d'une erreur;
4) `KERN_WARNING` Mise en garde;
5) `KERN_NOTICE` Etat normal mais notable;
6) `KERN_INFO` Message informatif;
7) `KERN_DEBUG` Message de débogage.

== dmesg ==

La commande `dmesg` permet d'interagir avec le tampon du noyau, et d'en lire les
messages via le fichier `/dev/kmsg`:
`dmesg` Afficher le tampon;
* `-C` Vidanger le tampon;
* `-c` Afficher et vidanger le tampon;
* `-D` Désactiver l'affichage sur les terminaux;
* `-E` Activer l'affichage sur les terminaux;
* `-H` Afficher les dates, paginer et colorier;
* `-k` Afficher les messages du noyau;
* `-l <level>[,...]` Afficher seulement les niveaux donnés;
* `-n <level>[,...]` Définir les niveaux envoyés sur les terminaux;
* `-u` Afficher les message envoyés depuis l'espace utilisateur;
* `-w` Suivre les messages;
* `-x` Décoder les facilities et niveaux.
