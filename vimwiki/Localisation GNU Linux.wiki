= Localisation GNU Linux =

La _localisation_, aussi dit les _paramètres régionaux_ ou _régionalisation_,
consiste à adapter l'interface en fonction des préférences culturelles et
linguistiques de l'utilisateur. Sous GNU/Linux, celle-ci est gérée directement
par l'implémentation GNU de la librairie standard C, ainsi que la seconde
librairie GNU gettext.

== Localisation par la librairie GNU C ==  

Un fichier de _definition de localisation_ (voir *locale(5)*) est compilé avec
la commande `localedef` (voir *localedef(1)*). Par défaut, les fichiers de
définitions sont stockés dans dans le dossier `/usr/share/i18n/locales/`
(`i18n` est l'abréviation d'internationalisation, 18 désigne le nombre de
lettres entre la première et dernière), et leurs versions compilées dans le
dossier `/usr/lib/locale/`. Le fichier `/usr/lib/locale/locale-archive`
contient les localisations compilées localement, il est chargé tel quel en
mémoire centrale par la librairie standard C.

Chaque définition est compilée avec en entrée:
* Un jeu de caractère (contenu dans le dossier `/usr/share/i18n/charmaps/`);
* Un ensemble d'informations sur la localisation divisé en catégories, tels les
  formats de dates, les unités de mesures et les notations numériques.

=== Catégories et variables d'environnement ===

Les informations de localisation sont divisées en _catégories_ (parfois dites
_sections_) contenants des paires clés-valeur:
* `LC_ADDRESS` Format des adresses géographiques et de régionalisation (plaque
  minéralogique, code postal...);
* `LC_COLLATE` Règles de tri alphanumérique et des expressions régulières;
* `LC_CTYPE` Interprétation des octets en caractères et règles de
  translittération (utilisées entre autres par `iconv`);
* `LC_IDENTIFICATION` Métadonnées sur la localisation;
* `LC_MONETARY` Format des chaînes numériques monétaires;
* `LC_MESSAGES` Langue des messages et des questions fermées (invite demandant
  une réponse par oui ou non);
* `LC_MEASUREMENT` Système de mesures utilisé (unités internationales ou
  impériales);
* `LC_NAME` Format des noms personnels;
* `LC_NUMERIC` Format des chaînes numériques;
* `LC_PAPER` Dimensions des feuilles d'impression (A4, US Letter...);
* `LC_PHONE` Format des numéros de téléphones;
* `LC_TIME` Format de l'heure et de la date;
* `LC_ALL` Pseudo-catégorie désignant toutes les autres.

Pour chacune de ces catégories, il existe une variable d'environnement du même
nom, indiquant la localisation couramment utilisées pour celle-ci (utilisée par
la fonction C `setlocale()`, voir *setlocale(3)*): sa valeur est le nom d'une
localisation *compilée localement* et donc présente dans `/usr/lib/locale/`. Ce
nom est au format `<lang>_<territory>[.charset][@<mod>]` dans lequel:
* `<lang>` est un code de langage au format ISO 639, tel `fr` pour le Français;
* `<territory>` est un code de pays au format ISO 3166, tel `FR` pour la
  France;
* `<charset>` est le jeu de caractères comme `UTF-8`;
* `<mod>` indique une version alternative de la localisation, par exemple la
  déclinaison `euro` de la régionalisation française.

Cela donne par exemple `C` pour la localisation standard C (Anglais avec
encodage ANSI X3.4-1968), et `fr_FR.UTF-8` pour la langue française, sur le
territoire français, compilée avec l'encodage UTF-8. 

Une règle de précédence entres variables d'environnement permet d'indiquer une
localisation par défaut sans nécessiter l'attribution individuelle de celles de
chaque catégories. La localisation d'une catégorie est définie par les
variables suivantes, dans leur ordre de priorité:
* La variable d'environnement `LC_ALL`;
* La variable d'environnement portant le nom de la catégorie (par exemple
  `LC_MESSAGES`);
* La variable d'environnement `LANG`.

On peut ainsi définir une localisation différente par catégorie. Une
utilisation commune de ce comportement est de garder la localisation courante
tout en utilisant le tri alphanumérique standard avec `LC_COLLATE=C`: les
autres catégories prendront la localisation de la variable `LANG`. Attention,
bien noter que `LC_ALL` est prioritaire à toutes les autres variables.

== GNU gettext == 

La librairie C _GNU gettext_ fournie la fonction `gettext()` (voir
*gettext(3)*), elle tentera de traduire la chaîne qui lui est passée en
argument de l'Anglais vers l'une des langue configurée par l'environnement.

La variable d'environnement `LANGUAGE` contient une liste de langues (au même
format que ceux de la variable `LANG`) dans leur ordre de préférence, séparées
par des `:`. Par exemple avec `LANGUAGE` valuée à `fr_FR:fr_CA`, `gettext()`
tentera de traduire vers l'une de ces langues, dans leur ordre de définition:
ici d'abord traduire en `fr_FR`, sinon en `fr_CA` (Français canadien), et si
aucune de ces langues n'est disponible, alors la chaîne sera affichée en
Anglais, telle que passée en argument.

== Commande locale ==

La commande `locale` (voir *locale(1)*) permet d'obtenir la localisation 
courante à partir des variables d'environnement.

Sans arguments, `locale` affichera la localisation *effective* des catégories:
celles dont la valeur est entre guillemets ne sont pas directement définies par
leurs variables d'environnement, mais par la précédence d'une autre (`LC_LANG`
et `LANG` si la variable de la catégorie n'est pas définie).

L'option `-a` (All) de `locale` listera les localisations disponibles sur le
système (donc compilées localement), les valeurs retournées sont légales pour
les variables d'environnement.

Avec comme argument le nom d'une catégorie, `locale` affichera les valeurs de 
ses clés (comme les formats, jeu de caractères...). L'option `-k` affichera en
plus le nom des clés (Keys). On peut donc avoir un résumé des informations sur
la localisation courante avec la commande `locale -k LC_IDENTIFICATION`.

TODO localectl status (systemd)
localectl set-local LANG=x (systemd)

TODO Debian: /etc/default/locale
RHEL: /etc/locale.conf

== Liens externes ==

* [[https://itectec.com/unixlinux/debian-difference-between-locale-archive-and-machine-object-files-in-usr-share-locale-lc_messages-directory/]]
* [[https://wiki.archlinux.org/title/Locale]]
