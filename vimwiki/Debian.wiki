= Debian =

TODO * [[Debian Preseeding d-i]],
* [[Configuration du clavier Debian]],
* Gestion des paquets:
	* [[dpkg et paquets Debian]],
	* [[Advanced Packaging Tool]],
	* [[Dépannage de Dpkg et APT]].

== Liens externes ==

* [[https://www.debian.org/doc/index.en.html]]
* [[https://www.debian.org/doc/debian-policy/index.html]]
