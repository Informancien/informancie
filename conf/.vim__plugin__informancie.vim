""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File:		informancie.vim
" Description:	Configuration pour Informancie
" Maintainer:	Léo DALECKI
" Home:		<https://framagit.org/Informancien/informancie>
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let s:cpo_bck = &cpoptions
set cpoptions&vim

" Configuration Vimwiki
let informancie = {}
let informancie.path = '%WIKI_PATH%'
let informancie.nested_syntaxes = {'sh': 'sh',
	\ 'ini': 'dosini',
	\ 'yaml': 'yaml',
	\ 'sql': 'sql'}

if exists('g:vimwiki_list')
	let g:vimwiki_list = add(g:vimwiki_list, informancie)
else
	let g:vimwiki_list = [informancie]
endif

" Configuration Vinscripts
let g:vinscripts_defaults = 1

let &cpoptions = s:cpo_bck
