#!/usr/bin/env -S awk -f

################################################################################
# File:		check_links
# Description:	AWK script to check for dead wiki links
# Maintainer:	Léo DALECKI
# Home:		<https://framagit.org/Informancien/informancie>
################################################################################

function next_link() { # Remove current link from $0
	$0 = gensub(/.+]]/, "", 1) # Remove first link from $0
}

BEGIN {
	print "file:lineno:missing"
}

BEGINFILE {
	wiki_dir = gensub(/\/.+$/, "", 1, FILENAME) 
}

{ 
	# Link matching regex:
	# \[\[ Beginning of link
	# ([a-z]+:)? Match optionnal prefix like 'file:'
	# ([^|#\]]+) Match link target until '|', '#', ']' or backquote
	# .*\]\] Rest and end of link
	while (match($0, /\[\[([a-z]+:)?([^|#\]\`]+).*\]\]/, a)) {

		file = wiki_dir "/" a[2] # Wiki file

		if (! a[1]) # Append .wiki extension if there is none
			file = file ".wiki"
		else if (a[1] ~ /^https?:/) { # Ignore external links
			next_link()
			continue
		}

		if (getline line < file == -1) # Check if file exists
			print FILENAME ":" FNR ":" file

		next_link()
	}
}
