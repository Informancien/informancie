################################################################################
# File:		Makefile
# Description:	Wiki management Makefile
# Maintainer:	Léo DALECKI
# Home:		<https://framagit.org/Informancien/informancie>
################################################################################

################################################################################
# Environnement
################################################################################

# Wiki's name
WIKI_NAME := Informancie

# Wiki's directory name
WIKI_DIR := vimwiki

# Wiki files extension
WIKI_EXT := wiki

# Configuration templates directory
CONF_DIR := conf

# Environnement variables can overidden in the `envvars` file
-include envvars

export WIKI_NAME WIKI_DIR CONF_DIR

################################################################################
# Variables
################################################################################

SHELL := $(shell which bash)
SCRIPTS_DIR := scripts
WIKI_FILES := $(WIKI_DIR)/*.$(WIKI_EXT)

################################################################################
# Targets
################################################################################

.ONESHELL:
.PHONY: install uninstall check_todos check_links check

install: # Configure wiki for local use
	@$(SCRIPTS_DIR)/conf_local install

uninstall: # Remove coniguration installed by `install` target
	$(SCRIPTS_DIR)/conf_local uninstall

check_todos: # Check for TODOs
	@printf 'file:lineno:todo\n'
	! grep -n '^TODO' $(WIKI_FILES)

check_links: # Check for internal dead links
	@! $(SCRIPTS_DIR)/check_links $(WIKI_FILES)

check: check_todos check_links # Run all wiki sanity checks
